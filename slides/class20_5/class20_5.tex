\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}

\newcommand{\vname}[1]{\mathtt{#1}}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
%\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
\renewcommand{\P}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}
\renewcommand{\ss}{~\mid~}

\newcommand{\sB}{\mathtt{burglar}}
\newcommand{\sE}{\mathtt{quake}}
\newcommand{\sA}{\mathtt{alarm}}
\newcommand{\sJ}{\mathtt{john}}
\newcommand{\sM}{\mathtt{mary}}

\title{Probabilistic Machine Learning}
\subtitle{Graphical Models and Bayes Nets}
\author{Marek Petrik}
\date{April 26, 2021}


\begin{document}

\begin{frame}
\maketitle
\vspace{-1.5in}
\begin{center}
\textbf{Based on}: Stuart J. Russell and Peter Norvig (2010). Artificial Intelligence A Modern Approach, 3rd edition. \\
\emph{See also}: P. Murphy, K. (2012). Machine Learning: A Probabilistic Perspective. Chapter~10.
\end{center}
\end{frame}

\begin{frame}{Today}
\begin{enumerate}
 \item Bayesian Machine Learning Models
 \vfill
 \item Probabilistic Programming: Stan
\end{enumerate}
\end{frame}

\begin{frame} {Conditional Independence}
    \begin{itemize}
        \item Independent random variables
        \[ \P[X,Y] = \P[X] \P[Y] \]
        \item Convenient, but not true often enough
        \pause
        \item \textbf{Conditional} independence
        \[ X \bot Y | Z \Leftrightarrow \P[X,Y | Z] = \P[X | Z] \P[Y|Z] \]
        \item Use conditional independence in machine learning
    \end{itemize}
\end{frame}

\begin{frame} {Bayes Nets}
    \begin{center}
        Graphical representation of conditional independence \\[1cm]
        \includegraphics[width=0.7\linewidth]{../figs/class19/directed_alarm.png}
    \end{center}
\end{frame}

\begin{frame} {Inference Formalized}
    \begin{itemize}
        \item Mary called, what is the probability of an earthquake?
        \[ P[\sE | \sM, \neg \sJ] = \frac{P[\sE, \sM, \neg \sJ]}{\P[\sM, \neg \sJ]} \]
        \vfill
        \item If there is an earthquake, what is the probability Mary calls?
        \[ P[\sM | \sE] = \frac{P[\sM, \sE]}{\P[\sE]} \]
    \end{itemize}
\end{frame}

\begin{frame} {Variables}
    \begin{enumerate}
        \item \textbf{Observable}: what is known
        \item \textbf{Query}: what is sought
        \item \textbf{Hidden} or latent: unknown
    \end{enumerate}
    What is hidden and observable?
    \begin{enumerate}
        \item Mary called, what is the probability of an earthquake?
        \item If there is an earthquake, what is the probability John calls?
        \item The alarm rings, is it earthquake or burglary?
    \end{enumerate}
    Hidden variable introduce computational complexity
\end{frame}

\begin{frame} \frametitle{Inference Formalized}
Mary called, what is the probability of an earthquake?
\[ \P[\sE \ss \sM, \neg \sJ] = \frac{P[\sE, \sM, \neg \sJ]}{\P[\sM, \neg \sJ]} \]
\vfill
If there is an earthquake, what is the probability Mary calls?
\[ \P[\sM \ss \sE] = \frac{P[\sM, \sE]}{\P[\sE]} \]
\end{frame}

\begin{frame} \frametitle{Variables}
\begin{enumerate}
	\item \textbf{Observable}: what is known
	\item \textbf{Query}: what is sought
	\item \textbf{Hidden} or latent: unknown
\end{enumerate}
What is hidden and observable?
\begin{enumerate}
	\item Mary called, what is the probability of an earthquake?
	\item If there is an earthquake, what is the probability John calls?
	\item The alarm rings, is it earthquake or burglary?
\end{enumerate}
Hidden variables introduce computational complexity
\end{frame}


\begin{frame}{Efficient Inference Methods}
\begin{enumerate}
 \item \textbf{Exact methods}: Variable elimination, compute
 \[\P[B=\vname{yes} \ss J=\vname{call}] \]
 \vfill
 \item \textbf{Sampling Methods}: Markov Chain Monte Carlo variations, approximate
 \[\P[B=\vname{yes} \ss J=\vname{call}] \]
 \vfill
 \item \textbf{Optimization-based Methods}: Variational inference, Compute or approximate Maximum A Posteriori (MAP) solution
 \[ \max_{b \in \{ \vname{yes}, \vname{no} \}} \P[B=b \ss J=\vname{call}] \]
\end{enumerate}
\vfill
\begin{small}
\emph{See}: Gelman, A., Carlin, J. B., Stern, H. S., Rubin, D. B. (2014). Bayesian Data Analysis. (3rd ed.)
\end{small}
\end{frame}

\begin{frame}{Inference and Machine Learning}
Many machine learning methods just perform inference:
\begin{enumerate}
 \item Linear and logistic regression
 \item LASSO and ridge regression
 \item LDA and QDA
\end{enumerate}
\vfill
Learning Bayesian Networks also just inference on parameters of distributions!
\end{frame}


\begin{frame}{Linear Regression as Inference}
Simple linear regression ($\epsilon_1, \epsilon_2, \ldots$ are independent) for sample $i$:
\[ Y_i = \beta_0 + \beta_1 \cdot X_i + \epsilon_i \]
$Y_1, Y_2, \ldots$ dependent but conditionally independent $Y_1 \bot Y_2 \ss Z$
\begin{center}
\begin{tikzpicture}[->,>=stealth,shorten >=1pt,auto,node distance=1.5cm,semithick]
\tikzstyle{hid} = [circle,draw=black,fill=white]
\tikzstyle{obs} = [circle,draw=black,fill=lightgray]
\node[hid] (beta0) {$\beta_0$};
\node[hid] (beta1) [right of=beta0] {$\beta_1$};
\node[obs] (y1) [below of=beta0] {$Y_1$};
\node[obs] (y2) [right of=y1] {$Y_2$};
\node[obs] (y3) [right of=y2] {$Y_3$};
\node[obs] (x1) [below of=y1] {$X_1$};
\node[obs] (x2) [right of=x1] {$X_2$};
\node[obs] (x3) [right of=x2] {$X_3$};
\path (beta0)  edge (y1) (beta1) edge (y1) (x1) edge (y1);
\path (beta0)  edge (y2) (beta1) edge (y2) (x2) edge (y2);
\path (beta0)  edge (y3) (beta1) edge (y3) (x3) edge (y3);
\end{tikzpicture}
\end{center}
Compute $\P[\beta \ss x_i, y_i]$ or MAP $\max_{\beta} \P[\beta \ss x_i, y_i]$. \pause Linear regression: uniform distribution over $\beta$, Ridge regression: Normal distribution over $\beta$
\end{frame}


\begin{frame}{Probabilistic Machine Learning Models}
\begin{itemize}
	\item Naive Bayes
	\vfill
	\item Markov Chains
	\vfill
	\item Hidden Markov Chains, Kalman Filter
	\vfill
	\item Undirected Graphical Models, Markov Random Fields
	\vfill
	\item Gaussian Processes
	\vfill
	\item Mixture of Distributions (unsupervised)
\end{itemize}
\end{frame}

\begin{frame}{Naive Bayes Model}
Closely related to QDA and LDA
\begin{center}
\begin{tikzpicture}[->,>=stealth,shorten >=1pt,auto,node distance=2cm,semithick]
\tikzstyle{hid} = [circle,draw=black,fill=white]
\tikzstyle{obs} = [circle,draw=black,fill=lightgray]

\node[obs] (r) {Region};
\node[obs] (ic) [below of=r] {I-Count};
\node[obs] (ec) [left of=ic] {E-Count};
\node[obs] (ca) [right of=ic] {C-Angle};

\path (r) edge (ic);
\path (r) edge (ec);
\path (r) edge (ca);
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}{Fitting Structured Models}
\centering
\includegraphics[width=\linewidth]{../figs/class19/samanthas_modified.pdf}
\end{frame}

\begin{frame}{Many Features Influenced By Observed Variable}
\centering
\includegraphics[width=\linewidth]{../figs/class20/samantha2.pdf}
\end{frame}

\begin{frame}{Naive Bayes Model}
\begin{center}
\begin{tikzpicture}[->,>=stealth,shorten >=1pt,auto,node distance=2cm,semithick]
\tikzstyle{hid} = [circle,draw=black,fill=white]
\tikzstyle{obs} = [circle,draw=black,fill=lightgray]

\node[obs] (r) {$Y_1$};
\node[obs] (ic) [below of=r] {$X_2$};
\node[obs] (ec) [left of=ic] {$X_1$};
\node[obs] (ca) [right of=ic] {$X_3$};

\path (r) edge (ic);
\path (r) edge (ec);
\path (r) edge (ca);
\end{tikzpicture}
\end{center}
\vfill
Classification:
\[ \P[y_1, x_1, x_2, x_3] = \P[y_1] \cdot \prod_{i=1}^3 \P[x_i \ss y] \]
Similar to LDA and QDA
\end{frame}

\begin{frame}{Many Features Influenced By Un-observed Variable}
\centering
But the data is not labeled!\\
\includegraphics[width=\linewidth]{../figs/class20/samantha2.pdf}
\end{frame}

\begin{frame}{Mixture of Distributions}
\begin{center}
\begin{tikzpicture}[->,>=stealth,shorten >=1pt,auto,node distance=2cm,semithick]
\tikzstyle{hid} = [circle,draw=black,fill=white]
\tikzstyle{obs} = [circle,draw=black,fill=lightgray]

\node[hid] (r) {$Y_1$};
\node[obs] (ic) [below of=r] {$X_2$};
\node[obs] (ec) [left of=ic] {$X_1$};
\node[obs] (ca) [right of=ic] {$X_3$};

\path (r) edge (ic);
\path (r) edge (ec);
\path (r) edge (ca);
\end{tikzpicture}
\vfill
Prediction:
\end{center}
\[ \P[y_1, x_1, x_2, x_3] = \P[y_1] \cdot \prod_{i=1}^3 \P[x_i \ss y] \]
Fit using Expectation-Maximization (non-convex likelihood function) or using MCMC
\end{frame}



\begin{frame}{Markov Chain}

\begin{itemize}
	\item 1st order Markov chain:
	\begin{center}
		\includegraphics[width=0.8\linewidth]{../figs/class19/markov1.png}
	\end{center}
	\vfill
	\item 2nd order Markov chain:
	\begin{center}
		\includegraphics[width=0.8\linewidth]{../figs/class19/markov2.png}
	\end{center}
\end{itemize}
\end{frame}

\begin{frame}{Uses of Markov Chains}
\begin{itemize}
		\item Time series prediction
		\item Simulation of stochastic systems
		\item Inference in Bayesian nets and models
		\item Many others \ldots
\end{itemize}
\end{frame}

\begin{frame}{Hidden Markov Models}

\begin{center}
	\includegraphics[width=0.8\linewidth]{../figs/class19/hmm.png}
\end{center}

Used for:
\begin{itemize}
\item Speech and language recognition
\item Time series prediction
\item \textbf{Kalman filter}: version with normal distributions used in GPS's
\end{itemize}
\end{frame}


\begin{frame}{Gaussian Processes}
Also known as Kriging, represents Gaussian linear regression with kernels
\begin{center}
	\includegraphics[width=0.8\linewidth]{../figs/class19/chi_feng_gp_regression.png}
	{\tiny Source:\url{https://livingthing.danmackinlay.name/gaussian_processes.html}}
\end{center}
\end{frame}

\begin{frame}{Probabilistic Modeling Languages}

\begin{itemize}
	\item Powerful frameworks to describe a probabilistic independence model and run MCMC
	\vfill
	\item Popular modeling and inference frameworks:
	\begin{itemize}
		\item JAGS
		\item BUGS, WinBUGS, OpenBUGS
        \item PyMC3, PyMC4, NumPyro
		\item Stan
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Stan Examples}
\begin{enumerate}
    \item Guide: {\tiny\url{https://mc-stan.org/docs/2_26/stan-users-guide/index.html}}
    \vfill
    \item Election forecast: {\tiny\url{https://projects.economist.com/us-2020-forecast/president}}
    \vfill
    \item Election code: {\tiny\url{https://github.com/TheEconomist/us-potus-model}}
\end{enumerate}
\end{frame}

\end{document}
