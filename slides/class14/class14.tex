\documentclass{beamer}

\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}



% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tr}{^\top}


\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\newenvironment{mprog}{\begin{equation}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation}}
\newenvironment{mprog*}{\begin{equation*}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation*}}
\newcommand{\stc}{\\[1ex] \subjectto}
\newcommand{\subjectto}{\mbox{s.t.} &}
\newcommand{\minimize}[1]{\min_{#1} &}
\newcommand{\maximize}[1]{\max_{#1} &}
\newcommand{\minsep}[2]{\min_{#1 \; \vline \; #2} &}
\newcommand{\maxsep}[2]{\max_{#1 \; \vline \; #2} &}
\newcommand{\cs}{\\[1ex] & }

\title{Support Vector Machines}
\subtitle{Maximum Margin Classifiers}
\author{Marek Petrik}
\date{Apr 1st, 2021}

\begin{document}

\begin{frame} \maketitle
\end{frame}


\begin{frame} \frametitle{Regression Trees}
\begin{itemize}
	\item Predict Baseball $\varname{Salary}$ based on $\varname{Years}$ played and $\varname{Hits}$
	\item Example:
	\begin{center}\includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter8/8.1}.pdf}\end{center}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Tree Partition Space}
\begin{center}
	\includegraphics[width=0.4\linewidth]{{../islrfigs/Chapter8/8.1}.pdf} \includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter8/8.2}.pdf}
\end{center}
\end{frame}

\begin{frame} \frametitle{Decision Tree Terminology}
\begin{itemize}
	\item Internal nodes
	\item Branches
	\item Leaves
\end{itemize}
\begin{center}\includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter8/8.1}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Learning a Decision Tree}
\begin{itemize}
	\item NP Hard problem
	\vfill
	\item<2-> Approximate algorithms (heuristics):
	\begin{itemize}
		\item ID3, C4.5, C5.0 (classification)
		\item CART (classification and regression trees)
		\item MARS (regression trees)
		\item \ldots
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Classification Trees: Metrics}
	\begin{itemize}
		\item $\hat{p}_{mk}$ is proportion of observations in $R_m$ in class $k$
		\item Partition to \emph{minimize} a metric:
		\begin{enumerate}
			\item Classification error rate (of the best class)
			\[ 1 - \max_k \hat{p}_{mk} = \min_k (1 - \hat{p}_{mk} ) \]
			Often too pessimistic in practice
			\item Gini (impurity) index (CART):
			\[ \sum_{k=1}^K \hat{p}_{mk} (1-\hat{p}_{m_k}) \]
			\item Cross-entropy (information gain) (ID3, C4.5):
			\[ - \sum_{k=1}^K \hat{p}_{mk} \log \hat{p}_{m_k} \]
		\end{enumerate}
		\item ID3, C4.5 do not prune
	\end{itemize}
\end{frame}

\begin{frame}{Comparison of Metrics}
	$\hat{p}_{mk}$ is proportion of observations in $R_m$ in class $k$
	\begin{enumerate}
		\item Classification error:
		$1 - \max_k \hat{p}_{mk} = \min_k (1 - \hat{p}_{mk} )$
		\item Gini:
		$\sum_{k=1}^K \hat{p}_{mk} (1-\hat{p}_{m_k})$
		\item Cross-entropy:
		$ - \sum_{k=1}^K \hat{p}_{mk} \log \hat{p}_{m_k} $
	\end{enumerate}
	\begin{center}
		\includegraphics[width=0.8\linewidth]{../figs/class13/metrics_plot.pdf}
	\end{center}
\end{frame}

\begin{frame} \frametitle{Bagging}
\begin{itemize}
	\item Stands for ``Bootstrap Aggregating''
	\item Construct multiple bootstrapped training sets:
	\[ T_1, T_2, \ldots, T_B \]
	\item Fit a tree to each one:
	\[  \hat{f}_1, \hat{f}_2, \ldots , \hat{f}_B \]
	\item Make predictions by averaging individual tree predictions
	\[  \hat{f}(x) = \frac{1}{B} \sum_{b=1}^B \hat{f}_b(x)\]
	\item Large values of $B$ are not likely to overfit, $B \approx 100$ is a good choice
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Random Forests}
\begin{itemize}
\item Many trees in bagging will be similar
\item Algorithms choose the same features to split on
\item Random forests help to address similarity:
\begin{itemize}
	\item At each split, choose only from $m$  randomly sampled features
\end{itemize}
\item Good empirical choice is $m = \sqrt{p}$
\end{itemize}
\begin{center}
\vspace{-10mm}
\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter8/8.10}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Gradient Boosting (Regression)}
\begin{itemize}
	\item Boosting uses all of data, not a random subset (usually)
	\item Also builds trees $\hat{f}_1, \hat{f}_2, \ldots$
	\item \alert{and} weights $\lambda_1, \lambda_2, \ldots$
	\item Combined prediction:
	\[ \hat{f}(x)  = \sum_{i} \lambda_i \hat{f}_i(x) \]
	\item  Assume we have $1 \ldots m$ trees and weights, next best tree?
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Gradient Boosting (Regression)}
\begin{itemize}
\item Just use \alert{gradient descent}
\item \textbf{Objective} is to minimize RSS (1/2):
\[ \frac{1}{2} \sum_{i=1}^n (y_i - f(x_i))^2 \]
\item \textbf{Objective} with the new tree $m+1$:
\[ \frac{1}{2} \sum_{i=1}^n \left(y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) - \alert{\hat{f}_{m+1}(x_i)} \right)^2 \]
\item Greatest reduction in RSS: \alert{gradient}
\[ y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) \approx \alert{\hat{f}_{m+1}(x_i)} \]
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Gradient Boosting}
\begin{itemize}
\item Greatest reduction in RSS: \alert{gradient}
\[ y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) \approx \alert{\hat{f}_{m+1}(x_i)} \]
\item \textbf{Fit new tree to the following target} (instead of $y_i$)
\[ y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) \]
\item Compute the weight $\lambda_{m+1}$ by \textbf{line search}:
\[ \min_{\tcb{\lambda_{m+1}}} \left(y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) - \tcb{\lambda_{m+1}}\alert{\hat{f}_{m+1}(x_i)} \right)^2 \]
\item And many other improvements
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Today}

\begin{enumerate}
	\item Linear support vector machines
	\item Kernels
	\item Kernel support vector machines

\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Separating Hyperplane}
	\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.1}.pdf}\end{center}
	\only<1>{Hyperplane: $\beta_0 + x\tr \beta = 0$}
	\only<2>{Blue: $\beta_0 + x\tr \beta > 0$}
	\only<3>{Red: $\beta_0 + x\tr \beta < 0$}
\end{frame}

\begin{frame} \frametitle{Question}
	\begin{itemize}
		\item Which other classification methods classify using a separating hyperplane?
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Best Separating Hyperplane}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.2}.pdf}\end{center}
	\begin{itemize}
		\item Data is separable
		\item Why would either one be better than others?
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Separating Hyperplane Methods}
	How is it computed?
	\begin{itemize}
		\item \textbf{Logistic regression}: \visible<2->{Maximum likelihood}
		\vfill
		\item<3-> \textbf{LDA}: \visible<4->{Maximum likelihood}
		\vfill
		\item<5-> \textbf{Support vector machines}: Maximum margin
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Maximum Margin Hyperplane}
	\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.3}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Computing Maximum Margin Hyperplane}
	\begin{center}\includegraphics[width=0.3\linewidth]{{../islrfigs/Chapter9/9.3}.pdf}\end{center}
	\begin{itemize}
		\item \textbf{Class labels}: $y_i \in \{-1,+1\}$ (\alert{not $\{0,1\}$})
		\item Solve a \textbf{quadratic program} (assume one of the features is a constant to get the equivalent of an intercept)
		\begin{mprog*}
		\maximize{\beta,M} \qquad M
		\stc y_i (\beta\tr x_i) \ge M
		\cs \| \beta \|_2 = 1
		\end{mprog*}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Non-separable Case}
	\begin{itemize}
		\item Rarely lucky enough to get separable classes
	\end{itemize}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter9/9.4}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Almost Unseparable Cases}
	\begin{itemize}
		\item Maximum margin can be brittle even when classes are separable
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.5}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Introducing Slack Variables}
	\begin{itemize}
		\item \textbf{Maximum margin classifier}
		\begin{mprog*}
			\maximize{\beta,M} \qquad M
			\stc y_i (\beta\tr x_i) \ge M
			\cs \| \beta \|_2 = 1
		\end{mprog*}
		\item \textbf{Support Vector Classifier} a.k.a Linear SVM
		\begin{mprog*}
			\maximize{\beta,M, \epsilon\ge 0} \qquad M
			\stc y_i (\beta\tr x_i) \ge (M - \alert{\epsilon_i})
			\cs \| \beta \|_2 = 1
			\cs \alert{\| \epsilon \|_1 \le C}
		\end{mprog*}
		\item Slack variables: $\epsilon$
		\item Parameter: $C$ \visible<2>{What if $C=0$?}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Effect of Decreasing Parameter $C$}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter9/9.7}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{What About Nonlinearity?}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.8}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Dealing with Nonlinearity}
	\begin{itemize}
		\item Introduce more features, just like with logistic regression
		\item It is possible to do better with SVMs
		\item \textbf{\underline{Primal} Quadratic Program}
		\begin{mprog*}
		\maximize{\beta,M} \qquad M
		\stc y_i (\beta\tr x_i) \ge M
		\cs \| \beta \|_2 = 1
		\end{mprog*}
		\item Equivalent \textbf{\underline{Dual} Quadratic Program}  (usually max-min, not here)
		\begin{mprog*}
			\maximize{\alpha\ge 0} \sum_{l=1}^M \alpha_l-\frac{1}{2} \sum_{j,k=1}^{M} \alpha_j \alpha_k y_j y_k \langle x_j, x_k \rangle
			\stc \sum_{l=1}^M \alpha_l y_l = 0
		\end{mprog*}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{SVM Dual Representation}
	\begin{itemize}
		\item \textbf{\underline{Dual} Quadratic Program}  (usually max-min, not here)
		\begin{mprog*}
		\maximize{\alpha\ge 0} \sum_{l=1}^M \alpha_l-\frac{1}{2} \sum_{j,k=1}^{M} \alpha_j \alpha_k y_j y_k \alert<2>{\langle x_j, x_k \rangle}
		\stc \sum_{l=1}^M \alpha_l y_l = 0
		\end{mprog*}
		\item \textbf{Representer theorem}: (classification test):
		\[f(z) = \sum_{l=1}^M \alpha_l y_l \alert<2>{\langle z, x_l\rangle} > 0\]
		\item<2-> Only need the inner product between data points
		\item<3-> Define a \textbf{kernel function} by projecting data to higher dimensions:
		\[ k(x_1,x_2) = \langle \phi(x_1), \phi(x_2) \rangle \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Kernelized SVM}
	\begin{itemize}
		\item \textbf{\underline{Dual} Quadratic Program} (usually max-min, not here)
		\begin{mprog*}
		\maximize{\alpha\ge 0} \sum_{l=1}^M \alpha_l-\frac{1}{2} \sum_{j,k=1}^{M} \alpha_j \alpha_k y_j y_k \alert{k(x_j, x_k )}
		\stc \sum_{l=1}^M \alpha_l y_l = 0
		\end{mprog*}
		\item \textbf{Representer theorem}: (classification test):
		\[f(z) = \sum_{l=1}^M \alpha_l y_l \alert{k(z, x_l)} > 0\]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Kernels}
	\begin{itemize}
		\item Polynomial kernel
		\[ k(x_1,x_2) = \left( 1 + x_1\tr x_2 \right)^d\]
		\item Radial kernel
		\[ k(x_1,x_2) = \exp\left( -\gamma \| x_1 - x_2 \|_2^2 \right) \]
		\item Many many more: Distance measure must be \textbf{positive definite}.

	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Polynomial and Radial Kernels}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.9}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{SVM vs LDA: Train}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.10}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{SVM vs LDA: Test}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.11}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Multiple Classes}
	\begin{itemize}
		\item One-vs-one
		\vfill
		\item One-vs-all
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{SVM vs Logistic Regression}
	\begin{itemize}
		\item \textbf{Logistic regression}: Minimize negative log likelihood
		\item \textbf{SVM}: Minimize \underline{hinge loss}
		\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.12}.pdf}\end{center}
	\end{itemize}
Use SVM when classes are well separated or there is a good \emph{kernel}
\end{frame}

\end{document}
