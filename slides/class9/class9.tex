\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage[ruled,linesnumbered]{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{nicefrac}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Lasso, Ridge Regression, and PCA/PCR}
\author{Marek Petrik}
\date{Mar 3rd, 2021}

\begin{document}

\begin{frame} \maketitle \end{frame}

\begin{frame} \frametitle{Previously in Machine Learning}
	\begin{itemize}
		\item How to choose the right features if we have (too) many options
		\vfill
		\item Methods:
		\begin{enumerate}
			\item Subset selection
			\item Regularization (shrinkage)
		\end{enumerate}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Best Subset Selection}
	\begin{itemize}
		\item Want to find a subset of $p$ features
		\item The subset should be \underline{small} and predict \underline{well}
		\item Example: $\varname{credit} \sim \varname{rating} + \varname{income} + \varname{student} + \varname{limit}$
	\end{itemize}
	\vspace{5mm}

	\begin{algorithm}[H] \caption{Best Subset Selection}
		$\mathcal{M}_0 \leftarrow $ \emph{null model} (no features)\;
		\For{$k = 1,2,\ldots, p$}{
			Fit all ${p \choose k} $ models that contain $k$ features \;
			$\mathcal{M}_k \leftarrow $ best of ${p \choose k}$ models according to a metric (CV error, $R^2$, etc)
		}
		\Return Best of $\mathcal{M}_0, \mathcal{M}_1, \ldots, \mathcal{M}_p$ according to metric above
	\end{algorithm}
\end{frame}

\begin{frame} \frametitle{Achieving Scalability}
	\begin{itemize}
		\item<1-> Complexity of \emph{Best Subset Selection}?
		\item<2-> Examine all possible subsets? How many?
		\item<3-> $O(2^p)$!
		\vfill
		\item<4-> Heuristic approaches:
		\begin{enumerate}
			\item \textbf{Stepwise selection}: Solve the problem approximately: \underline{greedy}
			\item \textbf{Regularization}: Solve a different (easier) problem: \underline{relaxation}
		\end{enumerate}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Which Metric to Use?}
	\begin{algorithm}[H] \caption{Best Subset Selection}
		$\mathcal{M}_0 \leftarrow $ \emph{null model} (no features)\;
		\For{$k = 1,2,\ldots, p$}{
			Fit all ${p \choose k} $ models that contain $k$ features \;
			$\mathcal{M}_k \leftarrow $ best of ${p \choose k}$ models according to a \alert{metric} (CV error, $R^2$, etc)
		}
		\Return Best of $\mathcal{M}_0, \mathcal{M}_1, \ldots, \mathcal{M}_p$ according to \alert{metric} above
	\end{algorithm}
	\begin{enumerate}
		\item<2-> \textbf{Direct error estimate}: Cross validation, precise but computationally intensive
		\item<3-> \textbf{Indirect error estimate}: Mellow's $C_p$:
		\[ C_p = \frac{1}{n} (\operatorname{RSS} + 2 d \hat{\sigma}^2) \text{ where } \hat{\sigma}^2 \approx \var[\epsilon] \]
		Akaike information criterion, BIC, and many others. Theoretical foundations
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Forward Stepwise Selection}
	\begin{itemize}
		\item Greedy approximation of \emph{Best Subset Selection}
		\item Iteratively add more features
		\item Example: $\varname{credit} \sim \varname{rating} + \varname{income} + \varname{student} + \varname{limit}$
	\end{itemize}
	\vspace{5mm}
	\begin{algorithm}[H] \caption{Forward Stepwise Selection}
		$\mathcal{M}_0 \leftarrow $ \emph{null model} (no features) \;
		\For{$k =  \alert{0}, 1,2,\ldots, \alert{p-1}$}{
			Fit all $p - k$ models that augment $\mathcal{M}_k$ by one new feature \;
			$\mathcal{M}_{k+1} \leftarrow $ best of $p-k$ models according to a metric (CV error, $R^2$, etc)
		}
		\Return Best of $\mathcal{M}_0, \mathcal{M}_1, \ldots, \mathcal{M}_p$ according to metric above
	\end{algorithm}
	\begin{itemize}
		\item<2-> Complexity? \visible<3>{$O(p^2)$}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Backward Stepwise Selection}
	\begin{itemize}
		\item Greedy approximation of \emph{Best Subset Selection}
		\item Iteratively remove features
		\item Example: $\varname{credit} \sim \varname{rating} + \varname{income} + \varname{student} + \varname{limit}$
	\end{itemize}
	\vspace{5mm}
	\begin{algorithm}[H] \caption{Backward Stepwise Selection}
		$\mathcal{M}_p \leftarrow $ \emph{full model} (all features) \;
		\For{$k =  \alert{p, p-1, \ldots, 1}$}{
			Fit all $k$ models that remove one feature from $\mathcal{M}_k$  \;
			$\mathcal{M}_{k-1} \leftarrow $ best of $k$ models according to a metric (CV error, $R^2$, etc)
		}
		\Return Best of $\mathcal{M}_0, \mathcal{M}_1, \ldots, \mathcal{M}_p$ according to metric above
	\end{algorithm}
	\begin{itemize}
		\item<2-> Complexity? \visible<3>{$O(p^2)$}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Regularization}
	\begin{enumerate}
		\item \textcolor{gray}{\textbf{Stepwise selection}: Solve the problem approximately}
		\vfill
		\item \textbf{Regularization}: Solve a different (easier) problem: \underline{relaxation}
		\begin{itemize}
			\item Solve a machine learning problem, but penalize solutions that use ``too much'' of the features
		\end{itemize}

	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Regularization}
	\begin{itemize}
		\item \textbf{Ridge regression} (parameter $\lambda$), $\ell_2$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j \beta_j^2  =\\
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2  + \lambda \sum_j \beta_j^2
		\end{gather*}
		\item \textbf{Lasso} (parameter $\lambda$), $\ell_1$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j |\beta_j| = \\
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 + \lambda \sum_j |\beta_j|
		\end{gather*}
		\item Approximations to the $\ell_0$ solution
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Why Lasso Works}
	\begin{itemize}
		\item Bias-variance trade-off
		\item Increasing $\lambda$ increases bias
		\item Example: all features relevant
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.8}.pdf}\\
		\textcolor{purple}{purple: test MSE}, black: bias, \textcolor{green}{green: variance} \\
		dotted (ridge) \end{center}
\end{frame}

\begin{frame} \frametitle{Why Lasso Works}
	\begin{itemize}
		\item Bias-variance trade-off
		\item Increasing $\lambda$ increases bias
		\item Example: some features relevant
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.9}.pdf}\\
		\textcolor{purple}{purple: test MSE}, black: bias, \textcolor{green}{green: variance} \\
		dotted (ridge) \end{center}
\end{frame}

\begin{frame} \frametitle{Regularization}
	\begin{itemize}
		\item \textbf{Ridge regression} (parameter $\lambda$), $\ell_2$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j \beta_j^2  =\\
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2  + \lambda \sum_j \beta_j^2
		\end{gather*}
		\item \textbf{Lasso} (parameter $\lambda$), $\ell_1$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j |\beta_j| = \\
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 + \lambda \sum_j |\beta_j|
		\end{gather*}
		\item Approximations to the $\ell_0$ solution
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Regularization: Constrained Formulation}
	\begin{itemize}
		\item \textbf{Ridge regression} (parameter $\lambda$), $\ell_2$ penalty
		\begin{gather*}
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2  \text{ subject to } \sum_j \beta_j^2 \le s
		\end{gather*}
		\item \textbf{Lasso} (parameter $\lambda$), $\ell_1$ penalty
		\begin{gather*}
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 \text{ subject to }  \sum_j |\beta_j| \le s
		\end{gather*}
		\item Approximations to the $\ell_0$ solution
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Lasso Solutions are Sparse}
	Constrained Lasso (left) vs Constrained Ridge Regression (right)
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.7}.pdf}\end{center}
	Constraints are blue, red are contours of the objective
\end{frame}

\begin{frame}\frametitle{How to Choose $\lambda$?}
\begin{itemize}
	\item<2-> Cross-validation
\end{itemize}
\visible<2>{
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.12}.pdf}\end{center}}
\end{frame}

\begin{frame} \frametitle{Standardizing Features}
\begin{itemize}
\item Regularization and PCR depend on scales of features
\item Good practice is to \emph{standardize} features to have \textbf{same variance}
\[ \tilde{x}_{ij} = \frac{x_{ij}}{\sqrt{\frac{1}{n}\sum_{i=1}^n (x_{ij} - \bar{x}_j)^2}} \]
\item Do not standardize features when they have the same units
\end{itemize}

\end{frame}

\begin{frame} \frametitle{Today}
	\begin{enumerate}
		\item Bayesian view of ridge regression and lasso
		\vfill
		\item Dimension reduction methods: Principal component regression
	\end{enumerate}
\end{frame}


\begin{frame}\frametitle{Bayes Theorem}
\begin{itemize}
	\item Classification from label distributions:
	\[ \Pr[Y = k \mid X = x] = \frac{\Pr[X = x \mid Y = k] \Pr[Y = k]}{\Pr[X = x]} \]
	\item Example:
	\begin{gather*} \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance} = \$100] = \\ \frac{\Pr[\varname{balance} = \$100 \mid \varname{default} = \operatorname{yes}] \Pr[\varname{default} = \operatorname{yes}]}{\Pr[\varname{balance} = \$100]}
	\end{gather*}
	\item Notation:
	\[ \Pr[Y = k \mid X = x] = \frac{\pi_k f_k(x)}{\sum_{l=1}^{K} \pi_l f_l(x)} \]
\end{itemize}
\end{frame}


\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
	\begin{itemize}
		\item \textbf{Likelihood}: Probability that data is generated from a model \only<2>{(\alert{i.i.d. assumption})}
		\only<1>{\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
		\only<2>{\[ \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \Pr[\tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] = \prod_{i=1}^n \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \]}
		\item Find the most likely model:
		\only<1>{\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
		\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \Pr[\tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}]  \]}
		\item Likelihood function is difficult to maximize
		\item Transform it using $\log$ (strictly increasing)
		\only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]}
		\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \log \ell(\tcr{\operatorname{\beta_0,\beta_1}}) =  \max_{\tcr{\operatorname{\beta_0,\beta_1}}}  \sum_{i=1}^n \log \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \]}
		\item Strictly increasing transformation preserves maximizer
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Bayesian Maximum A Posteriori (MAP) }
	\begin{enumerate}
		\item \textbf{Maximum likelihood}
		\[ \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
		\item \textbf{Maximum a posteriori estimate (MAP)}
		\[  \max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \visible<2->{=  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}} \]
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Maximum A Posteriori Estimate}
	\[
	\max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] =  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}
	\]
	\begin{itemize}
		\item \textbf{Prior}:
		\[  \Pr[\tcr{\operatorname{model}}] \]
		\item \textbf{Posterior}:
		\[ \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \]
		\item \textbf{Likelihood}:
		\[ \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \]
	\end{itemize}
\end{frame}

\begin{frame}{Estimating Coefficients: Maximum A Posteriori}
	\begin{itemize}
		\item \textbf{Posterior Probability}: Probability of the model being the true one \only<2>{(\alert{i.i.d. assumption})}
		\only<1>{\[ \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}} ]  \]}
		\only<2>{\[ \Pr[\tcr{\operatorname{\beta_0,\beta_1}} \mid \tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} ] \approx \prod_{i=1}^n \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \Pr[\tcr{\beta_0,\beta_1}] \]}
		\item Find the most likely model:
		\only<1>{\begin{align*} \max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] &= \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]} \\ &\approx \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \Pr[\tcr{\operatorname{model}}]  \end{align*}}
		\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \Pr[\tcr{\operatorname{\beta_0,\beta_1}} \mid \tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} ] \approx  \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \prod_{i=1}^n \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \Pr[\tcr{\beta_0,\beta_1}] \]}
		\item Ignore $\Pr[\tcb{\operatorname{data}}]$; it is constant
		\item Transform it using $\log$ (strictly increasing)
		\only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \]}
		\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}}  \sum_{i=1}^n \log \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] + \log \Pr[\tcr{\beta_0,\beta_1}] \]}
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Linear Regression as Maximum Likelihood}
\begin{itemize}
\item Linear model (Data $Y_i, X_i$, model $\beta$):
\begin{align*}
Y_i = \tcg{\beta_0} + X_i \tcg{\beta_1} + \tcr{\epsilon_i} \\
Y_i - \tcg{\beta_0} - X_i \tcg{\beta_1} = \tcr{\epsilon_i}
\end{align*}
\pause
\item Normally distributed errors (mean $0$, variance $1$):
\begin{align*}
f(\tcr{\epsilon_i}) &= \frac{1}{\sqrt{2\, \pi}}
\exp\left(-\frac{\tcr{\epsilon_i}^2}{2}\right) \\
&= \frac{1}{\sqrt{2\, \pi}}
\exp\left(-\frac{(Y_i - \tcg{\beta_0} - X_i \tcg{\beta_1})^2}{2}\right)
\end{align*}
\pause
\item Log Likelihood:
\[ \sum_{i = 1}^n \log f(\tcr{\epsilon_i}) \propto - \sum_{i = 1}^n \left(-\frac{(Y_i - \tcg{\beta_0} - X_i \tcg{\beta_1})^2}{2}\right) = - \operatorname{RSS} \]
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Ridge Regression as Normal Priors}
\begin{itemize}
\item Normal prior for $\tcg{\beta}$ values (mean $0$, variance $1$):
\[ g(\tcg{\beta_1}) = \frac{1}{\sqrt{2\, \pi}}
\exp\left(-\frac{\tcg{\beta_1}^2}{2}\right) \]
\pause
\item Log posterior:
\[
\log \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \propto \log \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] + \log \Pr[\tcr{\operatorname{model}}]
\]
\pause
\item Ridge regression log posterior:
\[ \sum_{i = 1}^n \log f(\tcr{\epsilon_i}) + \sum_{j=1}^p g(\tcg{\beta_j}) = -RSS - \sum_{j=1}^p \beta_j^2~. \]
\pause
\item \alert{Question}: What about the regularization constant $\lambda$?
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Dimensionality Reduction Methods}
	\begin{itemize}
		\item A different approach to model selection
		\item Maybe all features are not really necessary
		\item We have many features: $X_1, X_2, \ldots, X_p$
		\item Transform features to a \emph{smaller} number $Z_1, \ldots, Z_M$
		\vfill
		\item<2-> \textbf{Find} constants $\phi_{jm}$
		\item<2-> New features $Z_m$ are \textbf{linear combinations} of $X_j$:
		\[ Z_m = \sum_{j=1}^p \phi_{jm} X_j\]
		\item<3-> \textbf{Dimension reduction}: $M$ is much smaller than $p$
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Using Transformed Features}
	\begin{itemize}
		\item New features $Z_m$ are \textbf{linear combinations} of $X_j$:
		\[ Z_m = \sum_{j=1}^p \phi_{jm} X_j\]
		\item Fit linear regression model:
		\[ y_i = \theta_0 + \sum_{m=1}^M \theta_m z_{im} + \epsilon_i \]
		\item Run plain linear regression, logistic regression, LDA, or anything else
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Recovering Coefficients for Original Features}
	\begin{itemize}
		\item Prediction using transformed features
		\[ y_i = \theta_0 + \sum_{m=1}^M \theta_m z_{im} + \epsilon_i \]
		\item New features $Z_m$ are \textbf{linear combinations} of $X_j$:
		\[ Z_m = \sum_{j=1}^p \phi_{jm} X_j\]
		\item Consider prediction for data point $i$
		\[ \sum_{m=1}^M \theta_m z_{im} \visible<2->{=\sum_{m=1}^M \theta_m \sum_{j=1}^p \phi_{jm} x_{ij}} \visible<3->{=  \sum_{j=1}^p \alert<4>{\sum_{m=1}^M \theta_m \phi_{jm}} x_{ij}} \visible<4->{= \sum_{j=1}^p \alert<4>{\beta_j} x_{ij}} \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Dimension Reduction}
	\begin{enumerate}[<alert@+>]
		\item Reduce dimensions of features $Z$ from $X$
		\vfill
		\item Fit prediction model to compute $\theta$
		\vfill
		\item Compute weights for the original features $\beta$
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{How (and Why) Reduce Feaures?}
	\begin{enumerate}
		\item Principal Component Analysis (PCA)
		\vfill
		\item Partial least squares
		\vfill
		\item Also: many other non-linear dimensionality reduction methods
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Principal Component Analysis}
	\begin{itemize}
		\item \textbf{Unsupervised} dimensionality reduction methods
		\item Works with $n\times p$ \emph{data matrix} $\mathbf{X}$ (no labels)
		\item Correlated features: $\varname{pop}$ and $\varname{ad}$
	\end{itemize}
	\vspace{-9mm}
	\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter6/6.14}.pdf}\end{center}
	\vspace{-9mm}
\end{frame}

\begin{frame} \frametitle{1st Principal Component}
	\vspace{-9mm}
	\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter6/6.14}.pdf}\end{center}
	\vspace{-9mm}
	\begin{itemize}
		\item \textbf{1st Principal Component}: Direction with the largest variance
		\[ Z_1 = 0.839 \times (\varname{pop} - \overline{\varname{pop}}) + 0.544 \times (\varname{ad} - \overline{\varname{ad}}) \]
		\item<2-> Is this linear? \visible<3->{Yes, after \emph{mean centering}.}
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{1st Principal Component}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.15}.pdf}\\
	green line: 1st principal component, minimize distances to all points \end{center}
	\visible<2->{Is this the same as linear regression?}
	\visible<3->{\textbf{No}, like \emph{total least squares}.}
\end{frame}

\begin{frame} \frametitle{2nd Principal Component}
	\vspace{-9mm}
	\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter6/6.14}.pdf}\end{center}
	\vspace{-9mm}
	\begin{itemize}
		\item \textbf{2nd Principal Component}: Orthogonal to 1st component, largest variance
		\[ Z_2 = 0.544 \times (\varname{pop} - \overline{\varname{pop}}) - 0.839 \times (\varname{ad} - \overline{\varname{ad}}) \]
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{1st Principal Component}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter6/6.16}.pdf}\end{center}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter6/6.17}.pdf}\end{center}
\end{frame}


\begin{frame} \frametitle{Properties of PCA}
	\begin{itemize}
		\item No more principal components than features
		\vfill
		\item Principal components are perpendicular
		\vfill
		\item Principal components are eigenvalues of $\mathbf{X}^\top \mathbf{X}$
		\vfill
		\item Assumes normality, can break with heavy tails
		\vfill
		\item PCA depends on the scale of features
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{PCR: Principal Component Regression}
	\begin{enumerate}
		\item Use PCA to reduce features to a small number of principal components
		\item Fit regression using principal components
	\end{enumerate}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.18}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{PCR vs Ridge Regression \& Lasso}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.19}.pdf}\end{center}
	\begin{itemize}
		\item PCR selects combinations of all features (not feature selection)
		\item PCR is closely related to ridge regression
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{PCR Application}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.20}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Standardizing Features}
	\begin{itemize}
		\item Regularization and PCR depend on scales of features
		\item Good practice is to \emph{standardize} features to have \textbf{same variance}
		\[ \tilde{x}_{ij} = \frac{x_{ij}}{\sqrt{\frac{1}{n}\sum_{i=1}^n (x_{ij} - \bar{x}_j)^2}} \]
		\item Do not standardize features when they have the same units
		\item PCA needs mean-centered features
		\[ \tilde{x}_{ij} = x_{ij} - \bar{x}_j \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Partial Least Squares}
	A form of linear regression
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.21}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Interpreting Feature Selection}
	\begin{enumerate}
		\item Solutions may not be unique
		\item Must be careful about how we report solutions
		\item Just because one combination of features predicts well, does not mean others will not
	\end{enumerate}
\end{frame}



\end{document}
