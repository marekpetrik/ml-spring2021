\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{nicefrac}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Fitting Machine Learning Models}

\author{Marek Petrik}
\date{Feb 22, 2021}

\begin{document}

\begin{frame} \maketitle \end{frame}


\begin{frame}\frametitle{So Far in ML}
	\begin{itemize}
		\item Methods:
		\begin{enumerate}
			\item Linear regression
			\item Logistic regression
			\item LDA, QDA
			\item KNN
		\end{enumerate}
		\item Concepts:
		\begin{enumerate}
			\item Regression vs classification
			\item Parametric vs. nonparametric
			\item Generative vs. discriminative
		\end{enumerate}
	\end{itemize}

\end{frame}

\begin{frame}\frametitle{Discriminative vs Generative Models}
	\begin{itemize}
		\item \textbf{Discriminative models}
		\begin{itemize}
			\item Estimate conditional models $\Pr[Y \mid X]$
			\item Linear regression
			\item Logistic regression
		\end{itemize}
		\vfill
		\item \textbf{Generative models}
		\begin{itemize}
			\item Estimate joint probability $\Pr[Y, X] = \Pr[Y \mid X] \Pr[X]$
			\item Estimates not only probability of labels but also the features
			\item Once model is fit, can be used to generate data
			\item LDA, QDA, Naive Bayes
		\end{itemize}
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Algorithms to Live By}
\centering
\includegraphics[width=\linewidth]{../figs/class6/algotoliveby.png}
\end{frame}

\begin{frame} \frametitle{Machine Learning so Far}
\begin{tabular}{l|l}
	\textbf{Method Name} & \textbf{Uses Max Likelihood} \\
	\hline \hline
	Linear Discriminant Analysis (LDA) & \pause\tcg{Yes} \\
	\pause Quadratic Discriminant Analysis (QDA) & \pause \tcg{Yes} \\
	\pause Logistic Regression & \pause \tcg{Yes} \\
	\pause Least Squares, Linear Regression & \pause \tcg{Yes} \\
	\pause KNN & \pause \tcr{No}
\end{tabular}
\end{frame}

\begin{frame} \frametitle{Today: How Machine Learning Works}
    How to fit a model to data?
    \begin{enumerate}
        \item \textbf{Empirical Risk Minimization}:  Minimize some error metric on the training set (and hope it generalizes)
        \vfill
        \item \textbf{Maximum likelihood}: Parameters that maximize the probability of observed data
        \vfill
        \item \textbf{Maximum A-Posteriori (Bayesian)}: Compute probability over model parameters. Compute most likely parameters
    \end{enumerate}

\end{frame}


\begin{frame}{Fitting Machine Learning Models}

\begin{enumerate}
	\item \textbf{Empirical risk minimization}: KNN, SVM, decision trees, NN, linear regression
	\vfill
	\item \textbf{Maximum likelihood}: linear regression, logistic regression, LDA, QDA
	\vfill
	\item \textbf{MAP}: Lasso, ridge regression, Bayesian models (Latent Dirichlet Allocation)
\end{enumerate}
\end{frame}

\begin{frame}
    \Large
    \begin{enumerate}
        \item \textbf{Maximum likelihood}
        \vfill
        \item Maximum aposteriori
        \vfill
        \item Examples (why does this matter?)
    \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
	\begin{itemize}
		\item \textbf{Likelihood}: Probability that data is generated from a model \only<2>{(\alert{i.i.d. assumption})}
		\only<1>{\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
		\only<2>{\[ \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \Pr[\tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] = \prod_{i=1}^n \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \]}
		\item Find the most likely model:
		\only<1>{\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
		\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \Pr[\tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}]  \]}
		\item Likelihood function is difficult to maximize
		\item Transform it using $\log$ (strictly increasing)
		\only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]}
		\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \log \ell(\tcr{\operatorname{\beta_0,\beta_1}}) =  \max_{\tcr{\operatorname{\beta_0,\beta_1}}}  \sum_{i=1}^n \log \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \]}
		\item Strictly increasing transformation preserves maximizer
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Max-likelihood: Logistic Regression}
	\begin{itemize}
		\item \textbf{Parameters}:
		\[ \tcr{p}(X) = \frac{e^{\tcr{\beta_0}+\tcr{\beta_1} \, X}}{1+ e^{\tcr{\beta_0} + \tcr{\beta_1}\,X}}  \]
		\item \textbf{Data}: Features $x_i$ and labels $y_i$
		\item Likelihood:
		\[ \ell(\tcr{\beta_0},\tcr{\beta_1})  = \prod_{i : \tcb{y_i} =1} \tcr{p}(\tcb{x_i}) \prod_{i:\tcb{y_i}=0} (1-\tcr{p}(\tcb{x_i})) \]
		\item Log-likelihood:
		\[ \log \ell(\tcr{\beta_0},\tcr{\beta_1}) = \sum_{i: \tcb{y_i} =1} \log \tcr{p}(\tcb{x_i}) + \sum_{i:\tcb{y_i}=0} \log (1-\tcr{p}(\tcb{x_i})) \]
		\item Concave maximization problem
		\item Can be solved using gradient ascent (no closed form solution)
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Normal Distribution}
		Density function:
		\[ p(x) = \frac{1}{\sigma\sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} \]
		\vfill
		\begin{center}
			\includegraphics[width=0.8\linewidth]{../figs/class5/normal.pdf}
		\end{center}
\end{frame}

\begin{frame} \frametitle{Max-likelihood: Linear Regression}
\begin{itemize}
	\item \textbf{Parameters}:
	\[ \tcr{f}(X) = \tcr{\beta_0}+\tcr{\beta_1} \, X  \]
	\item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
	\item \textbf{Errors}: $\tcb{e_i} = \tcb{y_i} - \tcr{f}(\tcb{x_i}) = \tcb{y_i} - \tcr{\beta_0}-\tcr{\beta_1} \, \tcb{x_i}$
	\item Normally distributed errors: $\tcb{e_i} \sim \mathcal{N}(0,1)$
	\item Normal density function:
	\[ p(x) = \frac{1}{\sigma\sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} \]
	\item Probability of observing an error:
	\[ p(\tcb{e_i}) = \frac{1}{\sqrt{2\pi}} e^{-\frac{(\tcb{y_i} - \tcr{f}(\tcb{x_i}))^2}{2}} = \frac{1}{\sqrt{2\pi}} e^{-\frac{(\tcb{y_i} - \tcr{\beta_0}-\tcr{\beta_1} \, \tcb{x_i})^2}{2}} \]
\end{itemize}
\end{frame}


\begin{frame} \frametitle{Max-likelihood: Linear Regression (2)}
	\begin{itemize}
		\item \textbf{Parameters}:
		\[ \tcr{f}(X) = \tcr{\beta_0}+\tcr{\beta_1} \, X  \]
		\item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
		\item \textbf{Errors}: $e_i = y_i - f(x_i) \sim \mathcal{N}(0,1)$
		\item Likelihood of parameters:
		\[ \ell(\tcr{\beta_0},\tcr{\beta_1})  =\prod_{i=1}^n  p(\tcb{e_i}) =  \prod_{i=1}^n \frac{1}{\sqrt{2\pi}} e^{-\frac{(\tcb{y_i} - \tcr{\beta_0}-\tcr{\beta_1} \, \tcb{x_i})^2}{2}} \]
		\item Log-likelihood of parameters:
		\[ \log \ell(\tcr{\beta_0},\tcr{\beta_1}) \propto - \sum_{i=1}^n (\tcb{y_i} - \tcr{\beta_0}-\tcr{\beta_1} \, \tcb{x_i})^2 \]
		\item Familiar?
	\end{itemize}
\end{frame}

\begin{frame}{Max-likelihood: Some issues}
	\begin{enumerate}
		\item If something is known about $\beta$, how to incorporate it? Important with small data sets.
		\vfill
		\item What is the confidence in the values $\beta$ that are learned? Important when using the model?
	\end{enumerate}
\end{frame}

\begin{frame}
    \Large
    \begin{enumerate}
        \item Maximum likelihood
        \vfill
        \item \textbf{Maximum aposteriori}
        \vfill
        \item Examples (why does this matter?)
    \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Learning from Small Data Sets}
	\emph{How many Wildcat buses are there?}
	\begin{center}
		\includegraphics[width=\linewidth]{../figs/class6/wildcat.jpg}
	\end{center}
	\pause
	\emph{A good guess is about 80.}
\end{frame}



\begin{frame} \frametitle{Copernicus Principle}
\framesubtitle{aka Mediocrity Principle}
\centering
{\large ``if an item is drawn at random from one of several sets or categories, it's likelier to come from the most numerous category than from any one of the less numerous categories''} \\
\vspace{0.3in}
\includegraphics[width=0.7\linewidth]{../figs/class6/M31.jpg}\\
{\tiny Credit: By Adam Evans - M31, CC BY 2.0}
\end{frame}

\begin{frame}\frametitle{Bayes Theorem}
	\begin{itemize}
		\item Classification from label distributions:
		\[ \Pr[Y = k \mid X = x] = \frac{\Pr[X = x \mid Y = k] \Pr[Y = k]}{\Pr[X = x]} \]
		\item Example:
		\begin{gather*} \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance} = \$100] = \\ \frac{\Pr[\varname{balance} = \$100 \mid \varname{default} = \operatorname{yes}] \Pr[\varname{default} = \operatorname{yes}]}{\Pr[\varname{balance} = \$100]}
		\end{gather*}
		\item Notation:
		\[ \Pr[Y = k \mid X = x] = \frac{\pi_k f_k(x)}{\sum_{l=1}^{K} \pi_l f_l(x)} \]
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Bayesian Maximum A Posteriori (MAP) }
	\begin{enumerate}
		\item \textbf{Maximum likelihood}
		\[ \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
		\item \textbf{Maximum a posteriori estimate (MAP)}
		\[  \max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \visible<2->{=  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}} \]
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Maximum A Posteriori Estimate}
	\[
	\max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] =  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}
	\]
	\begin{itemize}
		\item \textbf{Prior}:
		\[  \Pr[\tcr{\operatorname{model}}] \]
		\item \textbf{Posterior}:
		\[ \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \]
		\item \textbf{Likelihood}:
		\[ \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \]
	\end{itemize}
\end{frame}

\begin{frame}{Estimating Coefficients: Maximum A Posteriori}
	\begin{itemize}
		\item \textbf{Posterior Probability}: Probability of the model being the true one \only<2>{(\alert{i.i.d. assumption})}
		\only<1>{\[ \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}} ]  \]}
		\only<2>{\[ \Pr[\tcr{\operatorname{\beta_0,\beta_1}} \mid \tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} ] \approx \prod_{i=1}^n \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \Pr[\tcr{\beta_0,\beta_1}] \]}
		\item Find the most likely model:
		\only<1>{\begin{align*} \max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}} ] &= \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]} \\ &\approx \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \Pr[\tcr{\operatorname{model}}]  \end{align*}}
		\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \Pr[\tcr{\operatorname{\beta_0,\beta_1}} \mid \tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} ] \approx  \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \prod_{i=1}^n \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \Pr[\tcr{\beta_0,\beta_1}] \]}
		\item Ignore $\Pr[\tcb{\operatorname{data}}]$; it is constant
		\item Transform it using $\log$ (strictly increasing)
		\only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \Pr[\tcr{\operatorname{model}}\mid \tcb{\operatorname{data}} ] \]}
		\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}}  \sum_{i=1}^n \log \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] + \log \Pr[\tcr{\beta_0,\beta_1} ] \]}
	\end{itemize}
\end{frame}

\begin{frame}{Posteriors for Biased Coin Estimation}
\centering

See section 3.3 in \emph{Murphy, K. (2012). Machine Learning: A Probabilistic Perspective. Machine Learning: A Probabilistic Perspective.}

\end{frame}

\begin{frame} \frametitle{MAP vs Max Likelihood}
	Computed models are the \textbf{same} when:\\
	\vfill
	\begin{enumerate}
		\item<2-> Prior is uniform. \emph{Uninformative priors}
		\item<3-> Amount of data available is large / infinite
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{MAP Advantages and Disadvantages}
\begin{itemize}
	\item \textbf{Advantages}:
	\begin{enumerate}
		\item Can use an informative \emph{prior}.
		\item Provides distribution of models, not just the most likely one
	\end{enumerate}
	\pause
	\vfill
	\item \textbf{Disadvantages}:
	\begin{enumerate}
		\item Requires a \emph{prior}. Where can we get it?
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Uninformative Priors}
Interpretation of $\Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}]$: If you use it for betting, you will not lose (Dutch Bookie Argument)\\
\vfill
\pause
\emph{Two Envelopes Paradox}: Uninformative uniform priors! \\
\url{https://en.wikipedia.org/wiki/Two_envelopes_problem} \\
\vspace{1cm}
\emph{Also see section 2.8 of \emph{Gelman, A., Carlin, J. B., Stern, H. S., \& Rubin, D. B. (2014). Bayesian Data Analysis. Chapman Texts in Statistical Science Series (3rd ed.).}}
\end{frame}

\begin{frame}
    \Large
    \begin{enumerate}
        \item Maximum likelihood
        \vfill
        \item Maximum aposteriori
        \vfill
        \item \textbf{Examples (why does this matter?)}
    \end{enumerate}
\end{frame}

\begin{frame}
    See other slides (maxlikelihood.pdf)
\end{frame}

\end{document}