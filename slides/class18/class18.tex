\documentclass{beamer}


\let\val\undefined
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Mixture of Distributions}
\subtitle{Expectation Maximization Edition}
\author{Marek Petrik}
\date{April 14th, 2021}


\begin{document}
\begin{frame} \maketitle
		\tiny{Some of the figures in this presentation are taken from "An Introduction to Statistical Learning, with applications in R"  (Springer, 2013) with permission from the authors: G. James, D. Witten,  T. Hastie and R. Tibshirani }
\end{frame}

\begin{frame} \frametitle{Clustering: Assumptions and Goals}
	\begin{itemize}
		\item Exists a method for measuring similarity between data points
		\item Some points are more similar than others
		\vfill
		\item<2-> \textbf{Want to identify similarity patterns}
		\begin{enumerate}
			\item Discover the different types of disease
			\item Market segmentation: Types of users that visit a website
			\item Discover movie or book genres
			\item Discover types of topics in documents
		\end{enumerate}
		\item<3-> Discover \textbf{latent} patterns that exist but may not be observed/observable
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{K-Means Clustering}
	\begin{itemize}
		\item Cluster data into \emph{complete} and \emph{non-overlapping} sets
		\item Example:
		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter10/10.5}.pdf}\end{center}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{K-Means Objective}
	\begin{itemize}
		\item $k$-th cluster: $C_k$
		\item $i$-th observation in cluster $k$: $i\in C_k$
		\item Find clusters that are homogeneous: $W(C_k)$ homogeneity of clusters
		\[ \min_{C_1,\ldots,C_K} \sum_{k=1}^K W(C_k) \]
		\item Define homogeneity as in-cluster variance
		\[ \min_{C_1,\ldots,C_K} \sum_{k=1}^K \left( \frac{1}{|C_k|} \sum_{i,i'\in C_k} \sum_{j=1}^{p} (x_{ij} - x_{i'j})^2 \right) \]
		\item Minimizing objective NP hard
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{K-Means Illustration}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter10/10.6}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Properties of K-Means}
    \pause
	\begin{itemize}
		\item \emph{Local minimum}: Does not necessarily find the optimal solution
		\item Multiple runs can result in different solutions
		\item Choose the result of the run with \underline{minimal objective}
		\item Cluster labels do not matter
	\end{itemize}
\end{frame}

\begin{frame}{Today: Mixture of Distributions by EM}

    \begin{enumerate}
        \item Motivation
        \vfill
        \item Mixture of Distributions
        \vfill
        \item Expectation Maximization
    \end{enumerate}
\end{frame}


\begin{frame}{Motivation: MMS Detecting Magnetopause}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/mms.png}
\end{frame}

\begin{frame}{MMS Plots}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/mms_plots.png}
\end{frame}

\begin{frame}{MMS Clock Angle (synthetic)}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/gaussian_uniform.pdf}
\end{frame}


\begin{frame}{MMS Clock Angle (synthetic)}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/gaussian_uniform_color.pdf}
\end{frame}

\begin{frame}{Clustering: Heat Pump Performance}
        \centering
        \includegraphics[width=\linewidth]{../figs/class18/heatpump_fake.pdf}
\end{frame}

\begin{frame}{Clustering: Heat Pump Performance}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/heatpump_fake_color.pdf}
\end{frame}

\begin{frame}{Mixture of Distributions}
    Two sets of random variables:
    \begin{enumerate}
        \item Observed: $X$  (clock angle)
        \item Hidden: $Z$  (MSP or MSH)
    \end{enumerate}
    \vfill
    Probability: $\Pr[X,Z \mid \theta ]$
    \vfill
    Gaussian distribution parameters: $\theta$
    \begin{enumerate}
        \item Probabilities of cluster: $\alpha_1, \ldots, \alpha_k$
        \item Means: $\mu_1, \ldots, \mu_k$
        \item Standard deviations: $\sigma_1^2, \ldots, \sigma_k^2$
    \end{enumerate}
\end{frame}

\begin{frame}{Gaussian Mixture: Observed $X$}
   \centering
   \includegraphics[width=\linewidth]{../figs/class18/gaussians_good.pdf}
\end{frame}

\begin{frame}{Gaussian Mixture: Hidden $Z$}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/gaussians_good_color.pdf}
\end{frame}

\begin{frame}{Gaussian Mixture: Observed $X$}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/gaussians_hard.pdf}
\end{frame}

\begin{frame}{Gaussian Mixture: Hidden $Z$}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/gaussians_hard_color.pdf}
\end{frame}

\newcommand{\model}{\tcr{\operatorname{model}}}
\newcommand{\latent}{\tcg{\operatorname{latent}}}
\newcommand{\data}{\tcb{\operatorname{data}}}

\begin{frame}{Fitting a Mixture of Distributions}
    \pause
    Maximum Likelihood
    \[ \max_{\model} \log \Pr[\data, \latent \mid\model] \]
    \pause
    \vfill
    Probability:
    \[
        \Pr[\tcb{X}, \tcg{Z} \mid \tcr{\alpha_i, \mu_i, \sigma^2_i} ] =
           \prod_{j=1}^n \alpha_{z_j} \cdot \frac{1}{\sigma_{z_j} \sqrt{2\pi} } \exp\left( \frac{-(x_j - \mu_{z_j})^2}{2\sigma_{z_j}^2} \right)
    \]
    \pause
    But latent variables are unknown
\end{frame}

\begin{frame}{Expectation Maximization}
    \begin{itemize}
        \item General method for dealing with latent features / labels
		\item Guess missing labels
        \item Suboptimal solution
    \end{itemize}
	\begin{gather*}
        \max_{\model} \log  \Pr[\data \mid \model] =  \max_{\model} \log \sum_{\latent} \Pr[\data, \latent \mid \model] 
    \end{gather*}
    Formally:
    \[
        \max_{\theta} \log \Pr[x \mid \theta] = \max_{\theta} \log \sum_{z \in \mathcal{Z}} \Pr[x, z \mid \theta] 
    \]
\end{frame}

\begin{frame}{EM Derivation}
    Iteratively improve on $\theta_n$ to get $\theta_{n+1}$ by solving
    \begin{align*}
        & \max_{\theta} \; \log \sum_{z} \Pr[x,z \mid \theta] \\
        &= \max_{\theta}\; \log \sum_{z} \frac{\Pr[z \mid x, \theta_n]}{\Pr[z \mid x, \theta_n]} \cdot \Pr[x,z \mid \theta]  \\
        &= \max_{\theta}\; \log \sum_{z} \Pr[z \mid x, \theta_n] \frac{\Pr[x,z \mid \theta]}{\Pr[z \mid x, \theta_n]} \\
        &\ge \max_{\theta}\; \sum_{z} \Pr[z \mid x, \theta_n] \log  \frac{\Pr[x,z \mid \theta]}{\Pr[z \mid x, \theta_n]} && \text{Jensen's} \\
        &\propto \max_{\theta}\; \sum_{z} \Pr[z \mid x, \theta_n] \log  \Pr[x,z \mid \theta] && \text{concave!}
    \end{align*}
\end{frame}

\begin{frame} {EM Big Idea}
    \[ \log \sum_{z} \Pr[x,z \mid \theta] \quad\approx\quad \sum_{z} \Pr[z \mid x, \theta_n] \log  \frac{\Pr[x,z \mid \theta]}{\Pr[z \mid x, \theta_n]} \]
    \vspace{7cm}
\end{frame}



\begin{frame}{EM Algorithm}

	\begin{enumerate}
		\item \textbf{E Step}: Estimate $\Pr[z \mid x, \theta_n]$ for all values of $z$. (Construct the lower bound)
		\item \textbf{M-Step}: Maximize the lower bound:
		\[ \theta_{n+1} = \arg\max_{\theta} \sum_{z} \Pr[z \mid x, \theta_n] \log  \Pr[x,z \mid \theta]  \]
		This can be solved using traditional MLE methods with weighted samples
	\end{enumerate}
\end{frame}


\begin{frame}{EM for Mixture of Gaussians}

    \[
    \log \Pr[\tcb{x_j}, \tcg{z_j} \mid \tcr{\alpha_i, \mu_i, \sigma^2_i} ] =
    \sum_{j=1}^n \alpha_{z_j} \cdot \frac{1}{\sigma_{z_j} \sqrt{2\pi} } \exp\left( \frac{-(x_j - \mu_{z_j})^2}{2\sigma_{z_j}^2} \right)
    \]
    \vfill
    Iterate $n$
	\begin{enumerate}
		\item Assign each observation $j$ \textbf{weights} based on the distances from the means
        \[\Pr[z_j \mid x_j, \theta_n] \propto \frac{1}{\sigma_{z_j} \sqrt{2\pi} } \exp\left( \frac{-(x_j - \mu_{z_j})^2}{2\sigma_{z_j}^2} \right) \]
		\item For each cluster, compute the centroid based on observation textbf{weights} of observations
        \[ \alpha_i = \sum_{j} \Pr[z_j \mid x_j, \theta_n] \cdot 1_{z_j = i}, \ldots  \]
	\end{enumerate}

\end{frame}

\begin{frame}{EM vs K-Means}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/ClusterAnalysis_Mouse.svg.png}\\
    {\tiny \url{https://en.wikipedia.org/wiki/Expectation\%E2\%80\%93maximization_algorithm}}
\end{frame}

\begin{frame}{MMS Plots}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/mms_plots.png}
\end{frame}

\begin{frame}{Clustering: Heat Pump Performance}
    \centering
    \includegraphics[width=\linewidth]{../figs/class18/heatpump_fake.pdf}
\end{frame}


\begin{frame}{Other Applications of EM}
	\begin{itemize}
		\item Very powerful and general idea!
        \item Mixtures of linear regressions
		\item Training with missing data for many model types
		\item Identifying confounding variables
		\item Solving difficult (complex) optimization problems
	\end{itemize}
    \vfill
    Other methods:
    \begin{itemize}
        \item Variational EM
        \item Markov Chain Monte Carlo
    \end{itemize}
    See: {\tiny\url{https://demonstrations.wolfram.com/ExpectationMaximizationForGaussianMixtureDistributions/}}
\end{frame}



\end{document}
