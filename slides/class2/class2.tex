\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{bigdelim,multirow}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\definecolor{maroon(x11)}{rgb}{0.69, 0.19, 0.38}
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tcm}[1]{\tc{maroon(x11)}{#1}}

\let\Var\undefined
\DeclareMathOperator{\RSS}{RSS}
\DeclareMathOperator{\Var}{Var}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Linear Regression}
\subtitle{Introduction to Machine Learning}
\author{Marek Petrik}
\date{February 3, 2021}

\begin{document}
\begin{frame}
	\maketitle
	\tiny{Some of the figures in this presentation are taken from "An Introduction to Statistical Learning, with applications in R"  (Springer, 2013) with permission from the authors: G. James, D. Witten,  T. Hastie and R. Tibshirani }
\end{frame}

\begin{frame}\frametitle{Last Class: We did and did \underline{not} cover}
    \begin{enumerate}
        \item Basic machine learning framework
        \[ Y = f(X) \]
        \item \textbf{Prediction vs inference}: predict $Y$ vs understand $f$
        \item \textbf{Parametric vs non-parametric}: linear regression vs k-NN
        \item \textbf{Classification vs regression}: k-NN vs linear regression
        \item Why we need to have a test set: overfitting
    \end{enumerate}
\end{frame}

\begin{frame} \frametitle{What is Machine Learning}
    \begin{itemize}
        \item Discover unknown function $\alert{f}$:
        \[ \tcb{Y} = \tcr{f}(\tcg{X}) + \epsilon \]
        \item $\tcr{f}$ = \textbf{hypothesis}
        \item $\tcg{X}$ = \textbf{features}, or predictors, or inputs
        \item $\tcb{Y}$ = \textbf{response}, or target
    \end{itemize}
    \[ \varname{MPG} = \alert{f}(\varname{Horsepower}) \]
\end{frame}

\begin{frame} \frametitle{Machine Learning Choices ...}
    \centering
    \includegraphics[width=\linewidth]{../figs/class1/ml_map.png}\\
    {\tiny Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}} \\[3mm]
    \textbf{End of the semester}: Know what, when, and why to use.
\end{frame}

\begin{frame}\frametitle{Why Estimate Hypothesis $f$?}
    \[ \varname{Sales} = f(\varname{TV}, \varname{Radio}, \varname{Newspaper}) \]
    \begin{center}\includegraphics[width=0.75\linewidth]{{../islrfigs/Chapter2/2.1}.pdf}\end{center}
    \begin{enumerate}
        \item<2-> \textbf{Prediction}: Make predictions about future: Best medium mix to spend ad money?
        \item<3-> \textbf{Inference}: Understand the relationship: What kind of ads work? Why?
    \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Prediction or Inference?}
    \begin{small}
        \begin{tabular}{|l|c|c|}
            \hline
            Application & Prediction & Inference \\
            \hline
            Identify risk factors for getting a disease &  & \visible<2->{\checkmark} \\
            Predict effectiveness of a treatment & \visible<3->{\checkmark} & \visible<3->{\checkmark} \\
            Recognize hand-written text & \visible<4->{\checkmark} & \\
            Speech recognition  & \visible<5->{\checkmark} & \\
            Determine why employees are leaving & \visible<6->{\checkmark} & \visible<6->{\checkmark} \\
            \hline
        \end{tabular}
    \end{small}
\end{frame}

\begin{frame}\frametitle{Types of Function $f$}
    \begin{columns}
        \begin{column}{0.5\linewidth}
            \centering
            \textbf{Regression}: continuous target
            \[ \tcr{f} : \tcg{\mathcal{X}} \rightarrow \tcb{\mathbb{R}} \]
            \begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.6}.pdf}\end{center}
        \end{column}
        \begin{column}{0.5\linewidth}
            \centering
            \textbf{Classification}: discrete target
            \[ \tcr{f} : \tcg{\mathcal{X}} \rightarrow \tcb{\{ 1,2,3,\ldots, k \}} \]
            \begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.13}.pdf}\end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame} \frametitle{Regression or Classification?}
    \begin{small}
        \begin{tabular}{|l|c|c|}
            \hline
            Application & Regression & Classification \\
            \hline
            Predict probability of getting a disease & \visible<2->{\checkmark} &  \\
            Predict effectiveness of a treatment & \visible<3->{\checkmark} &  \\
            Recognize hand-written text &  & \visible<4->{\checkmark} \\
            Speech recognition  &  & \visible<5->{\checkmark}\\
            Predict probability of an employee leaving & \visible<6->{\checkmark} &  \\
            \hline
        \end{tabular}
    \end{small}
\end{frame}


\begin{frame}\frametitle{How Good are Predictions?}
    \begin{itemize}
        \item Learned function $\hat{f}$
        \item Test data: ${(x_1,y_1), (x_2, y_2), \ldots}$
        \item \textbf{Mean Squared Error (MSE)}:
        \[
        \varname{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{f}(x_i))^2
        \]
%        \item This is the estimate of:
%        \[ \varname{MSE} = \Ex[(Y - \hat{f}(X))^2]  = \frac{1}{|\Omega|} \sum_{\omega\in\Omega} (Y(\omega) - \hat{f}(X(\omega)))^2 \]
        \item \textbf{Root Mean Squared Error (RMSE)}: Unchanged units
        \[
        \varname{RMSE} = \sqrt{\varname{MSE}}
        \]
        \item Important: Samples $x_i$ are i.i.d.
    \end{itemize}
\end{frame}

\begin{frame} \frametitle{Prediction Error: Training and test data sets}
    \centering
    \begin{tabular}{rrrll}
        \hline
        & \tcb{mpg} & \tcg{horsepower} & name & \\
        \hline
        1 & 18.00 & 130.00 & chevrolet chevelle malibu & \rdelim\}{3}{3mm}[test] \\
        2 & 15.00 & 165.00 & buick skylark 320 &\\
        3 & 18.00 & 150.00 & plymouth satellite &\\
        \hline \hline
        4 & 16.00 & 150.00 & amc rebel sst & \rdelim\}{7}{3mm}[training] \\
        5 & 17.00 & 140.00 & ford torino & \\
        6 & 15.00 & 198.00 & ford galaxie 500 & \\
        7 & 14.00 & 220.00 & chevrolet impala & \\
        8 & 14.00 & 215.00 & plymouth fury iii & \\
        9 & 14.00 & 225.00 & pontiac catalina & \\
        10 & 15.00 & 190.00 & amc ambassador dpl & \\
        \hline
    \end{tabular}
\end{frame}


\begin{frame} \frametitle{Do We Need Test Data?}
    Why not just test on the training data?
    \pause
    \begin{center}            \includegraphics[width=0.8\linewidth]{../figs/class1/auto_knn_compare.pdf}
    \end{center}
\end{frame}

\begin{frame} \frametitle{KNN Error}
    \includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter2/2.17}.pdf}
\end{frame}


%\begin{frame} \frametitle{Machine Learning}
%	\centering
%	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick,block/.style = {rounded corners, draw,fill=blue!1,align=center,inner sep=5}]
%	\node[block](data){Dataset};
%	\node[block,below of=data](algo){Machine Learning Algorithm};
%	\node[block,below of=algo](hypo){Hypothesis $\tcr{f}$};
%	\node[left of=hypo,xshift=-1cm](input) {Predictors $\tcg{X}$};
%	\node[right of=hypo,xshift=1cm](output) {Target $\tcb{Y}$};
%	\path (data) edge (algo)
%	(algo) edge (hypo)
%	(input) edge [dashed] (hypo)
%	(hypo) edge [dashed] (output);
%	\end{tikzpicture}
%\end{frame}

%\begin{frame} \frametitle{Errors in Machine Learning: World is Noisy}
%\begin{itemize}
%	\item World is too complex to model precisely
%	\item Many features are not captured in data sets
%
%	\item Need to allow for errors $\alert{\epsilon}$ in $f$: \\
%	\[ Y = f(X) + \alert{\epsilon} \]
%\end{itemize}
%\end{frame}


%\begin{frame}\frametitle{Why Methods Fail: Bias-Variance Decomposition}
%	Test data point predictors $x_0$ with target value $y_0$
%	\[ y_0 = f(x_0) + \alert{\epsilon} \]
%	Mean Squared Error of trained $\hat{f}$ can be decomposed as:
%		\[ \varname{MSE} = \Ex [(y_0 - \hat{f}(x_0))^2] = \underbrace{\Var[\hat{f}(x_0)]}_{\text{\tcg{Variance}}} + \underbrace{\Ex[\hat{f}(x_0) - f(x_0)]^2}_{\text{\tcb{Bias}$^2$}} + \Var[\tcr{\epsilon}] \]
%\begin{itemize}
%	\item \textbf{\tcb{Bias}}: How well would method work with an average dataset
%	\item \textbf{\tcg{Variance}}: How sensitive the output is to  different datasets
%\end{itemize}
%\end{frame}

%\begin{frame}\frametitle{Types of Function $f$}
%\begin{columns}
%	\begin{column}{0.5\linewidth}
%		\centering
%		\textbf{Regression}: continuous target
%		\[ f : \mathcal{X} \rightarrow \mathbb{R} \]
%		\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.6}.pdf}\end{center}
%	\end{column}
%	\begin{column}{0.5\linewidth}
%		\centering
%		\textbf{Classification}: discrete target
%		\[ f : \mathcal{X} \rightarrow \{ 1,2,3,\ldots, k \}\]
%		\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.13}.pdf}\end{center}
%	\end{column}
%\end{columns}
%\end{frame}


\begin{frame}\frametitle{Linear Regression}
	\begin{itemize}
		\item \textbf{Today:} What is linear regression
		\begin{enumerate}
            \item Statistics refresher
			\item What is linear regression
            \item Evaluating fit
			\item Solving linear regression
		\end{enumerate}
		\pause
        \vfill
		\item \textbf{Next time:} How to really make it work
		\begin{itemize}
			\item Multiple features
			\item Nonlinear linear regression
			\item How things may go wrong
			\item How to fix and diagnose it
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Today's Overview}
{\Large
\begin{enumerate}
    \item \textbf{Statistics refresher}
    \vfill
    \item What is linear regression
    \vfill
    \item Evaluating fit
    \vfill
    \item Solving linear regression
\end{enumerate}
}
\end{frame}

\begin{frame} \frametitle{Random Variables}
    \begin{itemize}
        \item Probability space $\Omega$: Set of all UNH students
        \item Random variable: $X(\omega) = \mathbb{R}$: Height
        \item Random variable: $Y(\omega) = \mathbb{R}$: GPA
    \end{itemize}
    \begin{center} \includegraphics[width=0.8\linewidth]{../figs/class2/students_uncorrelated.pdf} \end{center}
\end{frame}

\begin{frame}{Probability Distributions}
    \begin{enumerate}
        \item Bernoulli/Binomial distribution (discrete)
        \item Uniform distribution (continuous)
        \item Normal distribution (continuous)
        \item Laplace distribution (continuous)
        \item Poisson distribution (continuous)
    \end{enumerate}
\end{frame}


\begin{frame} \frametitle{Mean, Variance, Standard Deviation}
    \textbf{Mean}:
    \[ \Ex[X] = \frac{1}{|\Omega|} \sum_{\omega\in\Omega} X(\omega) \]
    \textbf{Variance}:
    \[ \Var[X] = \frac{1}{|\Omega|} \sum_{\omega\in\Omega} (X(\omega) - \bar{X})^2  = \frac{1}{|\Omega|} \sum_{\omega\in\Omega} (X(\omega) - \bar{X})(X(\omega) - \bar{X}) \]
    \textbf{Standard deviation}:
    \[ \sqrt{\Var[X]} \]
    \textbf{Covariance}:
    \[ \operatorname{Cov}(X,Y) = \frac{1}{|\Omega|} \sum_{\omega\in\Omega} (X(\omega) - \bar{X})(Y(\omega) - \bar{Y}) \]
\end{frame}

\begin{frame} \frametitle{Sample Statistics}
    Sample: $x_1, x_2, \ldots, x_n$ \\
    \vfill
    \textbf{Sample Mean}:
    \[ \bar{X} = \frac{1}{n} \sum_{i=1}^n x_i \]
    \textbf{Sample Variance (Biased!)}:
    \[ \sigma^2_X = \frac{1}{n} \sum_{i=1}^n (x_i - \bar{X})^2  \]
    \textbf{Sample Variance (Unbiased)}:
    \[ \sigma^2_X = \frac{1}{n-1} \sum_{i=1}^n (x_i - \bar{X})^2  \]
    Elementary proof: {\tiny\url{https://en.wikipedia.org/wiki/Variance\#Sample_variance}}
\end{frame}

\begin{frame}\frametitle{Correlation Coefficient}
    Measures dependence between two random variables $X$ and $Y$
    \[ r(X, Y) = \frac{\operatorname{Cov}(X,Y)}{\sqrt{\Var(X)}\sqrt{\operatorname{Var}(Y)}} \]
    \vfill
    \textbf{Correlation coefficient} $r$ is between $[-1,1]$
    \begin{description}
        \item[$0$:] Variables are not related
        \item[$1$:] Variables are perfectly  related (same)
        \item[$-1$:] Variables are negatively related (different)
    \end{description}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    {\tiny Plotting code in \texttt{slides/figs/class2/plots.R}}
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example0.pdf} \\[3mm]
    \visible<2>{Correlation: $0$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example1.pdf} \\[3mm]
    \visible<2>{Correlation: $1$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example1_stretched.pdf} \\[3mm]
    \visible<2>{Correlation: $1$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example2.pdf} \\[3mm]
    \visible<2>{Correlation: $-1$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example3.pdf} \\[3mm]
    \visible<2>{Correlation: $0.5$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example4.pdf} \\[3mm]
    \visible<2>{Correlation: $0.0$}
\end{frame}

\begin{frame} \frametitle{Today's Overview}
    {\Large
        \begin{enumerate}
            \item Statistics refresher
            \vfill
            \item \textbf{What is linear regression}
            \vfill
            \item Evaluating fit
            \vfill
            \item Solving linear regression
        \end{enumerate}
    }
\end{frame}

\begin{frame}{Why Linear Regression: Simple and Efficient}
\centering
\begin{minipage}{0.4\linewidth}
	\includegraphics[width=\linewidth]{../figs/class2/powder_mill_bridge.pdf}
	{\small Powder Mill Bridge, MA}
\end{minipage}
\hspace{0.15\linewidth}
\begin{minipage}{0.4\linewidth}
	\includegraphics[width=\linewidth]{../figs/class2/powder_mill_bridge_sens.pdf}
	{\small Strain gauges, PMB, MA}
\end{minipage}\\
\vspace{0.6cm}
\begin{minipage}{0.9\linewidth}
	{\small Bridge damage detection success (higher is better)} \\
	\includegraphics[width=\linewidth]{../figs/class2/kathryn_results.pdf}
	{\small Training time: LR: seconds, ANN: days} {\tiny[Kathryn Kaspar, A protocol for using long-term structural health monitoring data to detect and localize damage in bridges, UNH MS Thesis, 2018]}
\end{minipage}
\end{frame}

\begin{frame} \frametitle{Simple Linear Regression}
	\begin{itemize}
		\item There is only one feature:
			\[ \tcr{Y} \approx \tcg{\beta_0} + \tcb{\beta_1} X \qquad \tcr{Y} = \tcg{\beta_0} + \tcb{\beta_1} X  + \epsilon \]
		\item Example:
			\begin{center}\includegraphics[width=0.75\linewidth]{{../figs/class2/sales_tv_reg}.pdf}\end{center}
			\[ \tcr{\textrm{Sales}} \approx \tcg{\beta_0} + \tcb{\beta_1} \times \varname{TV} \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{How To Estimate Coefficients}
	\begin{itemize}
		\item No line that will have no errors on data $x_i$
		\item Prediction:
			\[ \tcr{\hat{y}_i} = \tcg{\hat\beta_0} + \tcb{\hat{\beta_1}} x_i\]
		\item Errors ($y_i$ are true values):
	 		\[ e_i = \tcm{y_i} - \tcr{\hat{y}_i}  \]
		\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter3/3.1}.pdf}\end{center}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Residual Sum of Squares}
	\begin{itemize}
		\item Residual Sum of Squares
		\[ \RSS = e_1^2 + e_2^2 + e_3^2 + \cdots + e_n^2  = \sum_{i=1}^n e_i^2\]
		\item Equivalently:
		\[ \RSS = \sum_{i=1}^n ( \tcm{y_i} - \tcg{\hat{\beta}_0} - \tcb{\hat\beta_1} x_i )^2 \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{$R^2$ Statistic and Correlation Coefficient $r$}
    \visible<2->{\[ R^2 = r^2 \]}
    \vfill
    \visible<3->{
        Correlation coefficient $r$:
        \begin{description}
            \item[$0$:] Variables are not related, $R^2 = 0$
            \item[$1$:] Variables are perfectly  related (same), $R^2 = 1$
            \item[$-1$:] Variables are negatively related (different), $R^2 = 1$
    \end{description}}
\end{frame}

\begin{frame} \frametitle{Why Minimize RSS}
    \begin{enumerate}
        \pause
        \item It is convenient: can be solved in closed form
        \vfill
        \pause
        \item Maximize likelihood when $Y = \beta_0 + \beta_1 X + \epsilon$ when $\epsilon \sim \mathcal{N}(0,\sigma^2)$
        \vfill
        \pause
        \item Best Linear Unbiased Estimator (BLUE): Gauss-Markov Theorem (ESL 3.2.2)
    \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Today's Overview}
    {\Large
        \begin{enumerate}
            \item Statistics refresher
            \vfill
            \item What is linear regression
            \vfill
            \item \textbf{Evaluating fit }
            \vfill
            \item Solving linear regression
        \end{enumerate}
    }
\end{frame}


\begin{frame}\frametitle{How Good is Fit?}
    \[ \varname{Sales} = f(\varname{TV}, \varname{Radio}, \varname{Newspaper}) \]
    \begin{center}\includegraphics[width=0.75\linewidth]{{../islrfigs/Chapter2/2.1}.pdf}\end{center}
    \vfill
    \begin{itemize}
        \item How well is linear regression predicting the training data?
        \item Can we be sure that TV advertising really influences the sales?
        \item \alert{Does RSS/MSE answer these questions?}
    \end{itemize}
\end{frame}


\begin{frame}\frametitle{$R^2$ Statistic}
\[ R^2 = 1 - \frac{\operatorname{RSS}}{\operatorname{TSS}} = 1 - \frac{\sum_{i=1}^n (\tcm{y_i} - \tcr{\hat{y}_i})^2 }{\sum_{i=1}^n (\tcm{y_i} - \tcb{\bar{y}})^2} \]
\begin{itemize}
\item RSS - residual sum of squares, TSS - total sum of squares
\item $R^2$ measures the goodness of the fit as a proportion
\item Proportion of data variance explained by the model
\item Extreme values:
\begin{description}
	\item[$0$:] Model does not explain data
	\item[$1$:] Model explains data perfectly
\end{description}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Example: TV Impact on Sales}
\begin{center}
\includegraphics[width=0.9\linewidth]{{../figs/class2/sales_tv_reg}.pdf} \\
\visible<2>{$R^2 = 0.61$}
\end{center}
\end{frame}

\begin{frame} \frametitle{Example: Radio Impact on Sales}
\begin{center}
\includegraphics[width=0.9\linewidth]{{../figs/class2/sales_radio}.pdf} \\
\visible<2>{$R^2 = 0.33$}
\end{center}
\end{frame}

\begin{frame} \frametitle{Example: Newspaper Impact on Sales}
	\begin{center}
	\includegraphics[width=0.9\linewidth]{{../figs/class2/sales_newspaper}.pdf} \\
	\visible<2>{$R^2 = 0.05$}
	\end{center}
\end{frame}


%\begin{frame} \frametitle{Statistical Bias in Inference}
%	\begin{itemize}
%	\item Assume a true value $\mu^\star$
%	\item Estimate $\mu$ is \textbf{unbiased} when $\Ex[\mu] = \mu^\star$
%	\item Standard mean estimate is \alert{unbiased} (e.g. $X \sim \mathcal{N}(0,1)$):
%	\[ \Ex\left[\frac{1}{n} \sum_{i=1}^n X_i \right] = 0 \]
%	\item Variance estimate is \alert{biased} (e.g. $X \sim \mathcal{N}(0,1)$):
%	\[ \Ex\left[\frac{1}{n} \sum_{i=1}^n (X_i - \bar{X})^2 \right] \neq 1 \]
%	where $\bar{X}$ is sample mean.
%	\end{itemize}
%\end{frame}
%
%
%\begin{frame} \frametitle{Example of Statistical Bias: UNH}
%	\begin{itemize}
%		\item Surveying height of UNH students (Mean: $170$cm, Std: $30$)
%		\item Each classroom has $5$ students
%		\item Mean computed for each classroom separately
%	\end{itemize}
%	\begin{center}
%		\includegraphics[width=0.8\linewidth]{../figs/class2/heights_mean.pdf}\\
%			{\tiny Plotting code in \texttt{slides/figs/class2/plots.R}}
%	\end{center}
%\end{frame}
%
%\begin{frame} \frametitle{Cumulative Means of Class Means}
%	\begin{center}
%		Classroom means (\textbf{unbiased}):\\\includegraphics[width=0.8\linewidth]{../figs/class2/heights_mean_cummean.pdf}
%	\end{center}
%	\alert{Not to be confused with \textbf{consistency}!}
%\end{frame}
%
%\begin{frame} \frametitle{Example of Statistical Bias: UNH}
%	\begin{itemize}
%		\item Surveying height of UNH students (Mean: $170$cm, Std: $30$)
%		\item Each classroom has $5$ students
%		\item Statistics separately for each classroom
%	\end{itemize}
%	\begin{center}
%		Variances (\textbf{biased})
%		\includegraphics[width=0.8\linewidth]{../figs/class2/heights_var.pdf}
%	\end{center}
%\end{frame}
%
%\begin{frame} \frametitle{Cumulative Means of Class Variances}
%	\[ \sigma^2_X = \frac{1}{n} \sum_{i=1}^n (x_i - \bar{X})^2  \]
%	\begin{center}
%		Variances (\textbf{biased}):\\\includegraphics[width=0.8\linewidth]{../figs/class2/heights_var_cummean.pdf}
%	\end{center}
%\end{frame}
%
%\begin{frame} \frametitle{Cumulative Means of Class Variances}
%	\[ \sigma^2_X = \frac{1}{n-1} \sum_{i=1}^n (x_i - \bar{X})^2  \]
%	\begin{center}
%		Variances (\textbf{unbiased}):\\\includegraphics[width=0.8\linewidth]{../figs/class2/heights_var_unb_cummean.pdf}
%	\end{center}
%\end{frame}

%\begin{frame} \frametitle{Linear Regression Estimate of $\beta$ is Unbiased}
%	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.3}.pdf}\end{center}
%	\begin{center}
%	Gauss-Markov Theorem (ESL 3.2.2)
%	\end{center}
%\end{frame}

\begin{frame} \frametitle{Today's Overview}
    {\Large
        \begin{enumerate}
            \item Statistics refresher
            \vfill
            \item What is linear regression
            \vfill
            \item Evaluating fit
            \vfill
            \item \textbf{Solving linear regression}
        \end{enumerate}
    }
\end{frame}

\begin{frame}\frametitle{Computing Best Linear Fit}
	\begin{center}\includegraphics[width=0.8\linewidth]{{../figs/class2/sales_tv_reg}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Minimizing Residual Sum of Squares}
	\[ \min_{\beta_0, \beta_1}\; \operatorname{RSS}  = \min_{\beta_0, \beta_1}\; \sum_{i=1}^n e_i^2 = \min_{\beta_0, \beta_1}\; \sum_{i=1}^n ( y_i - \beta_0 - \beta_1 x_i )^2 \]
	\only<1>{\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter3/3.2b}.pdf}\end{center}}%
	\only<2>{\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter3/3.2a}.pdf}\end{center}}
\end{frame}

\begin{frame} \frametitle{Solving for Minimal RSS}
	\[ \min_{\beta_0, \beta_1}\; \sum_{i=1}^n ( y_i - \beta_0 - \beta_1 x_i )^2 \]
	\begin{itemize}
		\item $\operatorname{RSS}$ is a \textbf{convex} function of $\beta_0,\beta_1$
		\item Minimum achieved when (recall the chain rule):
		\begin{align*}
			\frac{\partial \operatorname{RSS}}{\partial \beta_0} &= - 2 \sum_{i=1}^n ( y_i - \beta_0 - \beta_1 x_i ) = 0 \\
			\frac{\partial \operatorname{RSS}}{\partial \beta_1} &= - 2 \sum_{i=1}^n x_i ( y_i - \beta_0 - \beta_1 x_i ) = 0
		\end{align*}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Linear Regression Coefficients}
	\[ \min_{\beta_0, \beta_1}\; \sum_{i=1}^n ( y_i - \beta_0 - \beta_1 x_i )^2 \]
	Solution:
	\begin{align*}
		\beta_0 &= \bar{y} - \beta_1 \bar{x} \\
		\beta_1 &= \frac{\sum_{i=1}^{n} (x_i - \bar{x})(y_i - \bar{y}) }{\sum_{i=1}^{n} (x_i - \bar{x})^2} = \frac{\sum_{i=1}^{n} x_i (y_i - \bar{y}) }{\sum_{i=1}^{n} x_i (x_i - \bar{x})}
	\end{align*}
	where
	\[ \bar{x} = \frac{1}{n} \sum_{i=1}^n x_i \qquad \bar{y} = \frac{1}{n} \sum_{i=1}^n y_i \]
\end{frame}


%\begin{frame} \frametitle{Lecture Overview}
%\begin{enumerate}
%	\item What it is
%	\item Errors
%	\item Computing it
%	\item \textbf{Multiple features}
%\end{enumerate}
%\end{frame}
%
%\begin{frame} \frametitle{Multiple Linear Regression}
%	\begin{itemize}
%		\item Usually more than one feature is available
%		\[ \varname{sales} = \beta_0 + \beta_1 \times \varname{TV} + \beta_2 \times \varname{radio} + \beta_3 \times \varname{newspaper}  + \epsilon \]
%		\item In general:
%		\[ Y = \beta_0 + \sum_{j=1}^p \beta_j X_j \]
%	\end{itemize}
%\end{frame}
%
%\begin{frame} \frametitle{Multiple Linear Regression}
%	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.4}.pdf}\end{center}
%\end{frame}
%
%\begin{frame} \frametitle{Estimating Coefficients}
%	\begin{itemize}
%		\item Prediction:
%			\[ \hat{y}_i = \hat\beta_0 + \sum_{j=1}^p \hat\beta_j x_{ij} \]
%		\item Errors ($y_i$ are true values):
%			\[ e_i = y_i - \hat{y}_i  \]
%		\item Residual Sum of Squares
%			\[ \operatorname{RSS} = e_1^2 + e_2^2 + e_3^2 + \cdots + e_n^2  = \sum_{i=1}^n e_i^2\]
%		\item How to minimize RSS? \pause \textbf{Linear algebra}!
%	\end{itemize}
%\end{frame}

%\begin{frame} \frametitle{Lecture Overview}
%\begin{enumerate}
%	\item What it is
%	\item Errors
%	\item Computing it
%	\item Multiple features
%	\item \textbf{Inference}
%\end{enumerate}
%\end{frame}
%
%\begin{frame} \frametitle{Inference from Linear Regression}
%\begin{enumerate}
%	\item Are predictors $X_1, X_2, \ldots, X_p$ really predicting $Y$?
%	\item Is only a subset of predictors useful?
%	\item How well does linear model fit data?
%	\item What $Y$ should be predict and how accurate is it?
%\end{enumerate}
%\end{frame}

%\begin{frame} \frametitle{Hypothesis Testing}
%\begin{itemize}
%	\item Null hypothesis $H_0$:
%	\begin{center}There is no relationship between $X$ and $Y$ \end{center}
%	\[ \beta_1 = 0 \]
%	\vfill
%	\item Alternative hypothesis $H_1$:
%	\begin{center}There is some relationship between $X$ and $Y$ \end{center}
%	\[ \beta_1 \neq 0 \]
%	\vfill
%	\item Seek to reject hypothesis $H_0$ with small ``probability'' ($p$-value) of making a mistake
%	\item \alert{Not covered \& neccessary for this class!} No need to compute $p$-values
%\end{itemize}
%\end{frame}

%\begin{frame} \frametitle{Inference 1}
%	\begin{center}``Are predictors $X_1, X_2, \ldots, X_p$ really predicting $Y$?''\end{center}
%	\begin{itemize}
%		\item Null hypothesis $H_0$:
%		\begin{center}There is no relationship between $X$ and $Y$ \end{center}
%		\[ \beta_1 = 0 \]
%		\vfill
%		\item Alternative hypothesis $H_1$:
%		\begin{center}There is some relationship between $X$ and $Y$ \end{center}
%		\[ \beta_1 \neq 0 \]
%		\vfill
%		\item Seek to reject hypothesis $H_0$ with small ``probability'' ($p$-value) of making a mistake
%		\item See ISL 3.2.2 on how to compute F-statistic and reject $H_0$
%	\end{itemize}
%\end{frame}

%\begin{frame}{What Happens with More Predictors?}
%\begin{enumerate}
%	\item RSS on the training data (why?)
%	\pause
%	\item Bias (error with large data)
%	\pause
%	\item Variance (sensitivity to dataset)
%	\pause
%	\item MSE on the test data	(why?)
%\end{enumerate}
%\end{frame}
%
%\begin{frame}	\frametitle{Inference 2}
%	\begin{center} ``Is only a subset of predictors useful?'' \end{center}
%	\begin{itemize}
%		\item Compare prediction accuracy with only a subset of features
%		\item<2-> \textbf{RSS never increases with more features!}
%		\item<3-> Penalize using more features:
%		\begin{enumerate}
%			\item Mallows $C_p$
%			\item Akaike information criterion
%			\item Bayesian information criterion
%			\item Adjusted $R^2$
%		\end{enumerate}
%		\item<4-> Testing all subsets of features is impractical: $2^p$ options!
%		\item<5> \textbf{Cross-validation} is usually better! (later)
%	\end{itemize}
%\end{frame}
%
%
%\begin{frame} \frametitle{Inference 3}
%	\begin{center} ``How well does linear model fit data?'' \end{center}
%	\begin{itemize}
%		\item $R^2$ also always increases with more features (like RSS)
%		\item Is the model linear? Plot it:
%		\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter3/3.5}.pdf}\end{center}
%	\end{itemize}
%\end{frame}
%
%
%\begin{frame} \frametitle{Inference 4}
%	\begin{center}
%		``What $Y$ should we predict and how accurate is it?''
%	\end{center}
%	\begin{itemize}
%		\item The linear model is used to make predictions:
%		\[ y_{\text{predicted}} = \hat{\beta}_0 + \hat{\beta}_1 \, x_{\text{new}} \]
%		\item Can also predict a confidence interval (based on estimate on $\epsilon$):
%		\item<2> \textbf{Example}: Spent $\$100,000$ on TV and $\$20,000$ on Radio advertising
%		\begin{itemize}
%			\item \textbf{Confidence interval}: predict $f(X)$ (the average response):
%			\[ f(x) \in [10.985, 11,528] \]
%			\item \textbf{Prediction interval}: predict $f(X) + \epsilon$ (response + possible noise)
%			\[ f(x) \in [7.930,14.580] \]
%		\end{itemize}
%	\end{itemize}
%\end{frame}

\end{document}