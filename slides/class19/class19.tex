\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\newcommand{\vname}[1]{\mathtt{#1}}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
%\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
%\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
\renewcommand{\P}{\mathbb{P}}
\renewcommand{\ss}{~\mid~}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Probabilistic Machine Learning}
\subtitle{Graphical Models and Bayes Nets}
\author{Marek Petrik}
\date{April 19, 2021}


\newcommand{\sB}{\mathtt{burglar}}
\newcommand{\sE}{\mathtt{quake}}
\newcommand{\sA}{\mathtt{alarm}}
\newcommand{\sJ}{\mathtt{john}}
\newcommand{\sM}{\mathtt{mary}}

\begin{document}

\begin{frame}
\maketitle
\vspace{-1.5in}
\begin{center}
\textbf{Based on}: Stuart J. Russell and Peter Norvig (2010). Artificial Intelligence A Modern Approach, 3rd edition. \\
\emph{See also}: P. Murphy, K. (2012). Machine Learning: A Probabilistic Perspective. Chapter~10.
\end{center}
\end{frame}


\begin{frame}{Unsupervised Machine Learning}
    \pause
    \begin{enumerate}
        \item Dimensionality reduction: PCA
        \vfill
        \item Clustering: k-means
        \vfill
        \item Mixture of distributions: Expectation-Maximization
    \end{enumerate}
\end{frame}

\begin{frame}{Bayesian Machine Learning}

    \begin{itemize}
        \vfill
        \item \textbf{Today}: Bayesian Nets and Probability Distributions
        \vfill
        \item \textbf{Next lectures}: Bayesian ML
        \begin{enumerate}
            \item Naive Bayes
            \item Gaussian processes
            \item MCMC
        \end{enumerate}
    \end{itemize}
\end{frame}

\begin{frame}{Confidence in Predictions}
    \centering
    \includegraphics[width=0.7\linewidth]{../figs/class19/hurricane.png}
\end{frame}

\begin{frame}{Gaussian Process Interpolation}
    \centering
    \includegraphics[width=\linewidth]{../figs/class19/kriging.png}\\
    {\tiny\url{https://en.wikipedia.org/wiki/Kriging}}
\end{frame}

\begin{frame}{Combining Complex Data}
    \centering
    \includegraphics[width=\linewidth]{../figs/class19/elections.png}\\
    {\tiny\url{https://projects.economist.com/us-2020-forecast/president}}
\end{frame}


\begin{frame}{Why Bayesian Machine Learning}
\begin{itemize}
    \item Need a confidence in prediction:
    \begin{enumerate}
        \item Probability that a hurricane hits?
        \item Probability of stock market crash?
        \item Probability of treatment being effective?
    \end{enumerate}
    \vfill
    \item Need to incorporate structure prior knowledge
    \begin{enumerate}
     \item Dependencies between observed features
     \item Number of different types of observations
    \end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}{Probability Distributions}
\begin{itemize}
	\item \textbf{Discrete random variable}:
	\begin{itemize}
		\item Qualitative values
		\item \emph{Distributions}: Bernoulli, multinomial
	\end{itemize}
	\vfill
	\item \textbf{Continuous random variable}:
	\begin{itemize}
		\item Quantitative values
		\item \emph{Distributions}: Normal, Poisson, Beta, Dirichlet, Cauchy, \ldots
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame} {Distribution of Cars: Continuous or Discrete}
\begin{itemize}
	\item mpg
	\item cylinders
	\item displacement
	\item horsepower
	\item weight
	\item acceleration
	\item origin
\end{itemize}
\end{frame}

\begin{frame} {Probability Distribution}
\begin{itemize}
	\item Burglar alarm installed
	\item May also respond to earthquakes
	\item \emph{John} and \emph{Mary} are neighbors
	\item John always hears the alarm, but sometimes a phone
	\item Mary sometimes does not hear the alarm
\end{itemize}
\end{frame}

\begin{frame} {Random Variables}
\begin{enumerate}
	\item Burglar: Yes, No
	\item Earthquake: Yes, No
	\item Alarm: Yes, No
	\item John calls: Yes, No
	\item Mary calls: Yes, No
\end{enumerate}

\[ \P[\sB, \sE, \sA, \sJ, \sM] = ? \]
\end{frame}

\begin{frame} {Representing Probability Distribution}
\[ \P[\sB, \sE, \sA, \sJ, \sM] = ? \]
\vfill
\begin{center}
\begin{tabular}{ccccc|r}
	Burglar & Earthquake & Alarm & John & Mary & $\P$ \\
	\hline
	Yes & Yes & Yes & Yes & Yes & 0.01 \\
	Yes & Yes & Yes & Yes & No & 0.2 \\
	\ldots & & & & & \ldots \\
	No & No & No & No & No & 0.01
\end{tabular}
\end{center}
\pause
\vfill
\textbf{Grows exponentially with number of variables}
\end{frame}

\begin{frame} {Conditional Independence}
\begin{itemize}
	\item Independent random variables
	\[ \P[X,Y] = \P[X] \P[Y] \]
	\item Convenient, but not true often enough
	\pause
	\item \textbf{Conditional} independence
	\[ X \bot Y | Z \Leftrightarrow \P[X,Y | Z] = \P[X | Z] \P[Y|Z] \]
	\item Use conditional independence in machine learning
\end{itemize}
\end{frame}

\begin{frame} {Dependent but Conditionally Independent}
Events with coins of two types:
\begin{enumerate}
    \item Blue coin: always heads
    \item Red coin: always tails
\end{enumerate}
Events:
\begin{enumerate}
	\item $X$: Your first coin flip is heads
	\item $Y$: Your second flip is heads
	\item $Z$: Color of the coin
\end{enumerate}
\pause
\vfill
\begin{itemize}
	\item $X$ and $Y$ are \underline{not independent}
	\item $X$ and $Y$ are \underline{independent} given $Z$
\end{itemize}
\end{frame}

\begin{frame} {Independent but Conditionally Dependent}
Is this possible? \pause \textbf{Yes!}
Events with an unbiased coin:
\begin{enumerate}
\item $X$: Your first coin flip is heads
\item $Y$: Your second flip is heads
\item $Z$: The coin flips are the same
\end{enumerate}
\pause
\vfill
\begin{itemize}
\item $X$ and $Y$ are \underline{independent}
\item $X$ and $Y$ are \underline{not independent} given $Z$
\end{itemize}
\end{frame}

\begin{frame} {Bayes Nets}
\begin{center}
	 Graphical representation of conditional independence \\[1cm]
	 \includegraphics[width=0.7\linewidth]{../figs/class19/directed_alarm.png}
\end{center}
\end{frame}

\begin{frame} {Conditional Probability Table}
\begin{itemize}
	\item Burglary: $\P[\sB] = 0.001 $
	\item Earthquake: $\P[\sE] = 0.002$
	\item John: \\
	\begin{tabular}{c|c}
		$\sA$ & $\P[\sJ]$ \\
		\hline
		Yes & 0.9 \\
		No & 0.05
	\end{tabular}
	\item Mary: \\
	\begin{tabular}{c|c}
		$\sA$ & $\P[\sM]$ \\
		\hline
		Yes & 0.7 \\
		No & 0.01
	\end{tabular}
\end{itemize}
\end{frame}

\begin{frame} {Joint Probability Distribution}
\begin{gather*}
\P[\sB, \sE, \sA, \sJ, \sM] = \\
\P[\sB] \cdot \P[\sE] \cdot \P[\sA | \sB, \sE] \cdot \\
\P[\sJ | \sA] \cdot \P[\sM | \sA]
\end{gather*}
\pause
\begin{gather*}
\P[\sB, \neg\sE, \sA, \sJ, \sM] = \\
\P[\sB] \cdot \P[\neg\sE] \cdot \P[\sA | \sB, \neg\sE] \cdot \\
\P[\sJ | \sA] \cdot \P[\sM | \sA]
\end{gather*}
\end{frame}


\begin{frame} {Directed Graphical Models}
\begin{itemize}
	\item Represent complex structure of conditional independence
	\pause
	\item Node is independent of all predecessors \textbf{conditional} on parent value
	\[ x_s \; \bot \; x_{pred(s) \setminus pa(s)} \; | \; x_{pa(s)}  \]
	\begin{center}
		\includegraphics[width=0.5\linewidth]{../figs/class19/directed.png}
	\end{center}
\end{itemize}
\end{frame}

\begin{frame} {Inference: Answering Questions}
\begin{itemize}
	\item Mary called, what is the probability of an earthquake?
	\vfill
	\item John called, what is the probability of a burglary?
	\vfill
	\item If there is an earthquake, what is the probability I find out?
	\vfill
	\item The alarm rings, is it earthquake or burglary?
\end{itemize}
\end{frame}


\begin{frame} {Inference Formalized}
\begin{itemize}
	\item Mary called, what is the probability of an earthquake?
	\[ P[\sE | \sM, \neg \sJ] = \frac{P[\sE, \sM, \neg \sJ]}{\P[\sM, \neg \sJ]} \]
	\vfill
	\item If there is an earthquake, what is the probability Mary calls?
	\[ P[\sM | \sE] = \frac{P[\sM, \sE]}{\P[\sE]} \]
\end{itemize}
\end{frame}

\begin{frame} {Variables}
\begin{enumerate}
	\item \textbf{Observable}: what is known
	\item \textbf{Query}: what is sought
	\item \textbf{Hidden} or latent: unknown
\end{enumerate}
What is hidden and observable?
\begin{enumerate}
	\item Mary called, what is the probability of an earthquake?
	\item If there is an earthquake, what is the probability John calls?
	\item The alarm rings, is it earthquake or burglary?
\end{enumerate}
Hidden variable introduce computational complexity
\end{frame}

\begin{frame} {Marginalization}
\begin{center}
	\begin{tabular}{c|cc}
		& Rain 	& Sunny \\
		\hline
		Sox win 	& 0.1	& 0.5 	\\
		Sox lose 	& 0.3	& 0.1
	\end{tabular}
\end{center}
\vfill
Probability Red Sox win
\begin{align*}
\P[S=\vname{win}] &= \P[S=\vname{win},\,W=\vname{rain}] + \\
&+ \P[S=\vname{win},\,W=\vname{sunny}]
\end{align*}
Probability it will be sunny
\begin{align*}
\P[W=\vname{sunny}] &= \P[S=\vname{win},\,W=\vname{sunny}] + \\
&+ \P[S=\vname{lose},\,W=\vname{sunny}]
\end{align*}
\end{frame}

\begin{frame} {Marginalization Example}
Mary does not know my office number (cannot call):
\begin{gather*}
\P[\sB, \sE, \sA, \sJ] = \\\P[\sB, \sE, \sA, \sJ, \sM] + \\ +
\P[\sB, \sE, \sA, \sJ, \neg\sM]
\end{gather*}
\end{frame}

\end{document}
