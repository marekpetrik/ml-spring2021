\documentclass{beamer}

\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{nicefrac}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}


\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
\renewcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Logistic Regression}
\author{Marek Petrik}
\date{Feb 10, 2021}

\let\Var\undefined
\DeclareMathOperator{\RSS}{RSS}
\DeclareMathOperator{\Var}{Var}


\begin{document}

	\begin{frame} \maketitle
	\end{frame}

	\begin{frame} \frametitle{So Far in ML}
	\begin{itemize}
		\item Regression vs Classification
		\vfill
		\item Linear regression, KNN
		\vfill
		\item Bias-variance decomposition
		\vfill
		\item Nonlinear feature construction
		\vfill
		\item Quantitative vs qualitative features
	\end{itemize}
	\end{frame}

%	\begin{frame} \frametitle{Simple Linear Regression}
%	\begin{itemize}
%		\item We have only one feature
%		\[ Y \approx \beta_0 + \beta_1 X \qquad Y = \beta_0 + \beta_1 X + \epsilon \]
%		\item Example:
%		\begin{center}\includegraphics[width=0.75\linewidth]{{../figs/class2/sales_tv_reg}.pdf}\end{center}
%		\[ \varname{sales} \approx \beta_0 + \beta_1 \times \varname{TV} \]
%	\end{itemize}
%	\end{frame}
%
%	\begin{frame} \frametitle{Multiple Linear Regression}
%		\begin{center}\includegraphics[width=0.95\linewidth]{{../islrfigs/Chapter3/3.4}.pdf}\end{center}
%	\end{frame}

	\begin{frame}\frametitle{Understanding Flexibility: Bias-Variance Decomposition}
		\[ Y = f(X) + \alert{\epsilon} \]
		Mean Squared Error for point $x_0$ of trained $\hat{f}$ can be decomposed as:
		\[ \varname{MSE} = \Ex [(y_0 - \hat{f}(x_0))^2] = \underbrace{\Var[\hat{f}(x_0)]}_{\text{\tcg{Variance}}} + \underbrace{\Ex[\hat{f}(x_0) - f(x_0)]^2}_{\text{\tcb{Bias}$^2$}} + \Var[\tcr{\epsilon}] \]
		\begin{itemize}
			\item \textbf{\tcb{Bias}}: How well would method work with average dataset
			\item \textbf{\tcg{Variance}}: How much prediction varies with different datasets
		\end{itemize}
	\end{frame}

	\begin{frame}\frametitle{Types of Function $f$}
	\begin{columns}
		\begin{column}{0.5\linewidth}
			\centering
			\textbf{Regression}: continuous target
			\[ f : \mathcal{X} \rightarrow \mathbb{R} \]
			\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.6}.pdf}\end{center}
		\end{column}
		\begin{column}{0.5\linewidth}
			\centering
			\textbf{Classification}: discrete/qualitative target
			\[ f : \mathcal{X} \rightarrow \{ 1,2,3,\ldots, k \}\]
			\begin{center}\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.13}.pdf}\end{center}
		\end{column}
	\end{columns}
	\end{frame}

	\begin{frame} \frametitle{Today}
		\begin{itemize}
			\item Why not use linear regression for classification
			\item Logistic regression
			\item Maximum likelihood principle
			\item Reading:
			\begin{itemize}
				\item ISL 4.1-3
				\item ESL 2.6 (max likelihood)
			\end{itemize}
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Machine Learning Choices \ldots}
		\centering
		\includegraphics[width=\linewidth]{../figs/class1/ml_map.png}\\
		{\tiny Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}} \\[3mm]
	\end{frame}

%	\begin{frame} \frametitle{Examples of Classification}
%		\begin{enumerate}[<+>]
%			\item A person arrives at the emergency room with a set of symptoms
%			that could possibly be attributed to one of three medical conditions.
%			Which of the three conditions does the individual have?
%			\item An online banking service must be able to determine whether or not
%			a transaction being performed on the site is fraudulent, on the basis
%			of the user’s IP address, past transaction history, and so forth.
%			\item  On the basis of DNA sequence data for a number of patients with
%			and without a given disease, a biologist would like to figure out which
%			DNA mutations are deleterious (disease-causing) and which are not.
%		\end{enumerate}
%	\end{frame}

	\begin{frame} \frametitle{IBM Watson}
		\centering
		\includegraphics[width=0.8\linewidth]{../figs/class1/Watson_Jeopardy.jpg}
		{\tiny Fair use, https://en.wikipedia.org/w/index.php?curid=31142331} \\
		\vfill
		\textbf{Logistic regression} + clever function engineering
	\end{frame}

	\begin{frame} \frametitle{Qualitative or Quantitative?}
		\centering
		\begin{tabular}{|l|c|c|}
			\hline
			Value & Quantitative & Qualitative \\
			\hline
			Gender &  & \visible<2->{\checkmark} \\
			Temperature & \visible<3->{\checkmark} &  \\
			Grade &  \visible<4->{\checkmark} &   \\
			Color  &  & \visible<5->{\checkmark} \\
			Species & & \visible<6->{\checkmark} \\
			\hline
		\end{tabular}
	\end{frame}

	\begin{frame} \frametitle{Predicting Default}
		\[ \varname{default} \approx f(\varname{income}, \varname{balance}) \]
		\only<1>{\begin{center}\includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter4/4.1a}.pdf}\\{\small Red: default}\end{center}}
		\only<2>{\begin{center}Boxplot\\\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter4/4.1b}.pdf}\end{center}}
	\end{frame}

	\begin{frame} \frametitle{Casting Classification as Regression}
		\begin{itemize}
			\item \textbf{Regression}: $f: X \rightarrow \mathbb{R}$
			\item \textbf{Classification}: $f: X \rightarrow \{ 1,2,3 \}$
			\vfill
			\item<2-> But $\{1,2,3\} \subseteq \mathbb{R}$
			\item<2-> Do we even need classification?
			\vfill
			\item<3-> \textbf{Yes!} Analogous to indicator variables
			\item<3-> \textbf{Regression}: Values that are close are similar (quantitative values)
			\item<3-> \textbf{Classification}: Distance of classes is meaningless (qualitative values)
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Casting Classification as Regression: Example}
		\begin{itemize}
			\item Predict possible diagnosis:
				\[\{ \varname{stroke}, \varname{overdose}, \varname{seizure} \}\]
			\item Assign class labels:
			\[ Y = \begin{cases}
				1 &\text{if } \varname{stroke} \\
				2 &\text{if } \varname{overdose} \\
				3 &\text{if } \varname{seizure}
			\end{cases}~. \]
			\item Fit linear regression
			\item<2-> \textbf{Make predictions}: If uncertain whether symptoms point to $\varname{stroke}$ or $\varname{seizure}$, we predict $\varname{overdose}$
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Linear Regression for 2-class Classification}
	\[ Y =  \begin{cases}
	1 &\text{if } \varname{default} \\
	0 & \text{otherwise}
	\end{cases} \]
	\vfill
	\begin{center}
		Linear regression \hspace{20mm} Logistic regression\\
		\vspace{-7mm}
		\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.2}.pdf}
	\end{center}
	\[ \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] \]
	\end{frame}

	\begin{frame} \frametitle{Logistic Regression}
		\begin{itemize}
			\item Turn classification to regression: Predict \textbf{probability} of a class: $p(X)$
			\item Example: $p(\varname{balance})$ probability of default for person with $\varname{balance}$
			\item \textbf{Linear regression}:
			\[ p(X) = \beta_0 + \beta_1\, X  \]
			\item \textbf{Logistic regression}:
			\[ p(X) = \frac{e^{\beta_0+\beta_1 \, X}}{1+ e^{\beta_0 + \beta_1\,X}}  \]
			\item the same as:
			\[ \log\left( \frac{p(X)}{1-p(X)}\right) = \beta_0 + \beta_1\,X \]
			\item \textbf{Odds}: $\nicefrac{p(X)}{1-p(X)}$
		\end{itemize}
	\end{frame}


	\begin{frame} \frametitle{Logistic Function}
		\[ y = \frac{e^{x}}{1+ e^{x}} \]
		\begin{center}
			\vspace{-8mm}
			\includegraphics[width=0.85\linewidth]{../figs/class4/logistic.pdf}
			\vspace{-5mm}
		\end{center}
		\[ p(X) = \frac{e^{\beta_0+\beta_1 \, X}}{1+ e^{\beta_0 + \beta_1\,X}}  \]
	\end{frame}

	\begin{frame} \frametitle{Logit Function: Log Odds}
		\[ \log\left( \frac{p(X)}{1-p(X)}\right)  \]
		\begin{center}
			\vspace{-8mm}
			\includegraphics[width=0.8\linewidth]{../figs/class4/logit.pdf}
			\vspace{-5mm}
		\end{center}
		\[ \log\left( \frac{p(X)}{1-p(X)}\right) = \beta_0 + \beta_1\,X \]
	\end{frame}

	\begin{frame}{Fitting Logistic Regression}
		\begin{center}
			Predicts class probability as a linear model of log odds:
		\end{center}
		\[ \log\left( \frac{p(X)}{1-p(X)}\right) = \beta_0 + \beta_1\times X \]
		\pause
		\vspace{1.5cm}
		\begin{center}
			\textbf{How to compute $\beta_0$ and $\beta_1$?}
		\end{center}
	\end{frame}

	\begin{frame} \frametitle{What is a Good Fit in Classification}

	\begin{itemize}
		\item Mean squared error in regression:
		\[ \varname{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{f}(x_i))^2  \]
		\item Classification error:
		\[ \varname{CE} = \frac{1}{n} \sum_{i=1}^{n} \mathbf{1}\{y_i \neq \hat{f}(x_i)\}  \]
		\item Accuracy = 1-classification error
	\end{itemize}
	\end{frame}

	\begin{frame}{Fitting Regression Methods}
		\begin{itemize}
			\item \textbf{Linear regression}: Minimize MSE on training set
			\item \textbf{Logistic regression}: Minimize CE on training set\textbf{?}
			\pause
			\item Minimizing CE is NP-hard; rarely minimized
			\vfill
			\pause
			\item \textbf{Linear regression}: Maximize likelihood of training set
			\item \textbf{Logistic regression}: Maximize likelihood of training set
		\end{itemize}

	\end{frame}

	\begin{frame}{Identifying a Biased Coin}
	\begin{itemize}
		\item \textbf{Parameter}: $\tcr{p}$ predicts the \emph{probability} of heads
		\vfill
		\item Two predictions:
		\begin{enumerate}
			\item $\tcr{p} = 0.4$
			\item $\tcr{p} = 0.7$
		\end{enumerate}
		\vfill
  		\item Which one is better / more likely?
  		\vfill
		\item \textbf{Data}: $\tcb{h}$ heads and $\tcb{t}$ tails
	\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
		\begin{itemize}
			\item \textbf{Likelihood}: Probability that data is generated from a model \only<2>{(\alert{i.i.d. assumption})}
			\only<1>{\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
			\only<2>{\[ \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \Pr[\tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] = \prod_{i=1}^n \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \]}
			\item Find the most likely model:
			\only<1>{\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
			\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \Pr[\tcb{\operatorname{Y_1,Y_2,Y_2,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}]  \]}
			\item Likelihood function is difficult to maximize
			\item Transform it using $\log$ (strictly increasing)
			\only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]}
			\only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \log \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \sum_{i=1}^n \log \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \]}
			\item Strictly increasing transformation preserves maximizer
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Example: Maximum Likelihood}
		\begin{itemize}
			\item \textbf{Parameter}: Assume a coin with $\tcr{p}$ as the probability of \emph{heads}
			\item \textbf{Data}: \underline{$\tcb{h}$ heads, $\tcb{t}$ tails} (in some arbitrary order)
			\item The likelihood function is:
			\[ \ell(\tcr{p}) = \tcr{p}^{\tcb{h}} \, (1-\tcr{p})^{\tcb{t}}~.\]
			\begin{center}
				\vspace{-7mm}
				\includegraphics[width=0.85\linewidth]{../figs/class4/likelihood10.pdf}
			\end{center}
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Likelihood Function: 2 coin flips}
			\begin{center}
			\textbf{heads} $\tcb{h}=1$ \qquad \textbf{tails} $\tcb{t}=1$\\
			\vspace{-5mm}
			\includegraphics[width=\linewidth]{../figs/class4/likelihood1.pdf}
			\vspace{-5mm}
			\end{center}
	\end{frame}

	\begin{frame} \frametitle{Likelihood Function: 20 coin flips}
	\begin{center}
		\textbf{heads} $\tcb{h}=10$ \qquad \textbf{tails} $\tcb{t}=10$\\
		\vspace{-5mm}
		\includegraphics[width=\linewidth]{../figs/class4/likelihood10.pdf}
		\vspace{-5mm}
	\end{center}
	\end{frame}

	\begin{frame} \frametitle{Likelihood Function: 200 coin flips}
	\begin{center}
		\textbf{heads} $\tcb{h}=100$ \qquad \textbf{tails} $\tcb{t}=100$\\
		\vspace{-5mm}
		\includegraphics[width=\linewidth]{../figs/class4/likelihood100.pdf}
		\vspace{-5mm}
	\end{center}
	\end{frame}

	\begin{frame} \frametitle{Maximizing Likelihood}
		\begin{itemize}
			\item Likelihood function is not concave: hard to maximize
			\[ \ell(\tcr{p}) = \tcr{p}^{\tcb{h}} \, (1-\tcr{p})^{\tcb{t}}~.\]
			\item Maximize the log-likelihood instead
			\[ \log \ell(\tcr{p}) = \tcb{h}\,\log(\tcr{p}) + \tcb{t}\, \log(1-\tcr{p}) ~.\]
			\begin{center}
				\vspace{-9mm}
				\includegraphics[width=0.8\linewidth]{../figs/class4/loglikelihood.pdf}
			\end{center}
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Log-likelihood: Biased Coin}
		\begin{center}
			\textbf{heads} $h=20$ \qquad \textbf{tails} $t=50$\\
			\vspace{-5mm}
			\includegraphics[width=\linewidth]{../figs/class4/loglikelihood_biased.pdf}
		\end{center}
	\end{frame}

	\begin{frame} \frametitle{Maximize Log-likelihood}
		\begin{itemize}
			\item Log-likelihood:
			\[ \log \ell(\tcr{p}) = \tcb{h}\,\log(\tcr{p}) + \tcb{t}\, \log(1-\tcr{p}) ~.\]
			\vfill
			\item<2-> Maximum where derivative = 0
			\item<2-> Derivative:
			\[ \frac{d}{dp} \left(\tcb{h}\,\log(\tcr{p}) + \tcb{t}\, \log(1-\tcr{p}) \right) = \frac{\tcb{h}}{\tcr{p}} - \frac{\tcb{t}}{1-\tcr{p}} \]
			\vfill
			\item<3-> Maximum likelihood solution:
			\[ \tcr{p} = \frac{\tcb{h}}{\tcb{h}+\tcb{t}} \]
		\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Max-likelihood: Logistic Regression}
		\begin{itemize}
			\item \textbf{Parameters}:
			\[ \tcr{p}(X) = \frac{e^{\tcr{\beta_0}+\tcr{\beta_1} \, X}}{1+ e^{\tcr{\beta_0} + \tcr{\beta_1}\,X}}  \]
			\item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
			\item Likelihood:
			\[ \ell(\tcr{\beta_0},\tcr{\beta_1})  = \prod_{i : \tcb{y_i} =1} \tcr{p}(\tcb{x_i}) \prod_{i:\tcb{y_i}=0} (1-\tcr{p}(\tcb{x_i})) \]
			\item Log-likelihood:
			\[ \ell(\tcr{\beta_0},\tcr{\beta_1}) = \sum_{i: \tcb{y_i} =1} \log \tcr{p}(\tcb{x_i}) + \sum_{i:\tcb{y_i}=0} \log (1-\tcr{p}(\tcb{x_i})) \]
			\item Concave maximization problem
			\item Can be solved using gradient ascent (no closed form solution)
		\end{itemize}
	\end{frame}


	\begin{frame} \frametitle{Multiple Logistic Regression}
	\begin{itemize}
		\item No closed form solution
		\item Multiple features
		\[ p(X) = \frac{e^{\beta_0 + \beta_1 X_1 + \beta_2 X_2 + \ldots + \beta_n X_n}}{1 + e^{\beta_0 + \beta_1 X_1 + \beta_2 X_2 + \ldots + \beta_n X_n}}   \]
		\item Equivalent to:
		\[ \log \left( \frac{p(X)}{1-p(X)} \right) = \beta_0 + \beta_1 X_1 + \beta_2 X_2 + \ldots + \beta_n X_n \]
	\end{itemize}
	\end{frame}

	\begin{frame} \frametitle{Multinomial Logistic Regression}
		\begin{itemize}
			\item Predicting multiple classes:
			\begin{itemize}
				\item Medical diagnosis
				\[ Y = \begin{cases}
				1 &\text{if } \varname{stroke} \\
				2 &\text{if } \varname{overdose} \\
				3 &\text{if } \varname{seizure}
				\end{cases}~. \]
				\item Predicting which products customer purchases
			\end{itemize}
			\item Straightforward generalization of logistic regression to $N$ classes
			\[ \frac{e^{\beta_0 + \beta_1 X}} {1+e^{\beta_0 + \beta_1 X}} \quad \Rightarrow \quad \frac{e^{\beta_{1,0} + \beta_{1,1} X}} {1+e^{\beta_{1,0} + \beta_{1,1} X}+e^{\beta_{2,0} + \beta_{2,1} X}\ldots e^{\beta_{N,0} + \beta_{N,1} X}} \]
		\end{itemize}
	\end{frame}

\end{document}