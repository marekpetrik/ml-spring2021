library(ggfortify)
library(readr)
library(dplyr)
library(reshape2)
library(NMF)

theme_set(theme_light())

kahoot <- read_csv("kahoot.csv")

prcomp(~ . - Player, data = kahoot)

kh_b <- kahoot %>% mutate(across(starts_with("Q"), function(x){as.numeric(x > 0.1)} ))
kh_pca <- prcomp(~ . - Player - Q31, data = kh_b)

autoplot(kh_pca)


fact <- NMF::nmf(M, 7, "lee")

B <- basis(fact) 
C <- coef(fact) 

error <- mean(abs(M - round(B %*% C)))
cat("Error", error)

pdf("coefmap.pdf")
coefmap(fact)
dev.off()

pdf("basismap.pdf")
basismap(fact)
dev.off()
