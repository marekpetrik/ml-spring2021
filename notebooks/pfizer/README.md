# When does vaccine protection start?

## Data

Efficiency data reverse engineered from the plot on COVID19 vaccine efficiency. Cumulative infection: Figure 13 from <https://www.fda.gov/media/144246/download>.

The incubation period data comes from Figure 2A from <https://www.nejm.org/doi/full/10.1056/NEJMoa2001316>.

Reverse engineering tool: <https://apps.automeris.io/wpd/>

## Files 

Show cumulative case incidence as a function of the number of days since the first vaccine. It shows decimal days because it is interpolated from the plot. The raw data does not seem to be publicly available.

    - `placebo.csv`: incidence in the placebo group
    - `vaccine.csv`: incidence in the vaccinated group

The incubation period is also reverse-engineered and is in the file

    - `incubation.csv`: shows the relative frequency of the onset of symptomps since the infection