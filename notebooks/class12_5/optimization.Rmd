---
title: "Optimization"
output:
  html_notebook: default
  pdf_document: default
---

Simple gradient descent for a single variable. Code adapted from [Wikipedia](https://en.wikipedia.org/wiki/Gradient_descent). 
```{r}
gd.optimize <- function(par, obj, grad){
    alpha = 0.03    # stepsize
    iter = 500      # iterations count
    x.all = numeric(iter) # history
    x = par # initial value
    for(i in seq_len(iter)){
            x = x - alpha*grad(x)
            x.all[i] = x
            cat(x, ", ")
    }
    print(plot(x.all, type = "l"))
    return(x)
}
```


Linear regression with no features (only intercept) with $y = [2,4,5,3]$
```{r}
y <- c(2,4,5,3)
objFun <- function(beta) {0.5 * sum((beta - y)^2)}
gradient <- function(beta) {sum(beta - y)}
x.init <- runif(1, 0, 10)
gd.optimize(x.init, objFun, gradient)
```

Or using standard tools: Quasi-Newton method finds the solution in 4 steps:
```{r}
optim(x.init, objFun, gradient, method = "BFGS")
```

