---
title: "Introduction to R Notebooks"
output:
  pdf_document:
    fig_width: 3
    fig_height: 2
---

This notebook demonstrates how to plot data and write simple reports.

# R Markdown

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing code chunks below by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*.

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

LaTeX math notation also works: $Y \approx \beta_0 + \beta_1 \times X$. Or if you want to have an equation on a line all by itself:
$$ Y \approx \beta_0 + \beta_1 \times X $$

For a good reference on typing mathematical formulas using LaTeX, see, for example [this tutorial](https://en.wikibooks.org/wiki/LaTeX/Mathematics).

# Plotting

**Step 1**: Download data for the "Introduction to Statistical Learning" (*you may need to do this manually on non-linux operating systems*)
```{bash, cache=TRUE, results='hide'}
cd /tmp
wget https://www.statlearning.com/s/Advertising.csv
```

If the above does not work, you can instead run the following code to load the file from the web directly.
```{r}
if(!requireNamespace("readr")){install.packages("readr")}
ads <- readr::read_csv("https://www.statlearning.com/s/Advertising.csv")
```


**Step 2**: Load the dataset from the CSV
```{r, cache=TRUE}
if(!("ads" %in% ls())){ # check if not loaded already
  ads <- read.csv("/tmp/Advertising.csv")  
}
```

**Step 3**: Summarize the data
```{r}
summary(ads)
```

**Step 5**: Plot the dataset
```{r, fig.align='center'}
plot(ads$TV, ads$Sales,col='red',pch=20,xlab = "TV", ylab = "Sales")
```

**Step 6**: Or generate prettier plots  with `ggplot2`
```{r}
if("ggplot2" %in% rownames(installed.packages()) == FALSE) {install.packages("ggplot2")}
```

```{r,fig.align='center'}
library(ggplot2) #install.packages("ggplot2")
ad.plot <- ggplot(ads, aes(x=TV,y=sales)) + geom_point() + 
  geom_smooth(method="loess") + labs(x="TV",y ="Sales") + 
  theme_light()
ad.plot
```

**Step 7**: Have fun with themes

Replicate the ugly excel plots.
```{r,fig.align='center'}
library(ggthemes) #install.packages("ggthemes")
ad.plot + theme_excel()
```

Or try something different, like a Wall Street Journal theme.
```{r,fig.align='center'}
ad.plot + theme_wsj()
```

