---
title: "Example Keras Application"
output: html_notebook
---

# A Very Simple Example

See [https://keras.rstudio.com/](https://keras.rstudio.com/) for a tutorial in R and [https://keras.io/](https://keras.io/) for one in python. Even in R, see the python keras documentation. The R library is a wrapper around the python package.

This example does not work, but we will try to fix it in the class.

Load some useful libraries first.
```{r}
library(keras)
library(ISLR)
library(dplyr)
library(ggplot2)
if(!keras::is_keras_available()){install_keras()}
theme_set(theme_light())
```

Build a model of the neural network. The network is sequential because each layer feeds its outputs to the next layer in the sequence. We can provide the activation function for each layer of the network.
```{r}
model <- keras_model_sequential()
model %>% 
    layer_dense(units = 100, activation = "relu", input_shape = 5) %>% 
    layer_dense(units = 1, activation = "sigmoid")
```

Load a dataset and split it into training and test sets.
```{r}
dataset <- Auto
training_id <- sample.int(nrow(dataset), size = 0.7*nrow(dataset))
dataset_train <- dataset[training_id, ]
dataset_test <- dataset[-training_id, ]
```

Build matrices with the input data.
```{r}
X <- model.matrix(mpg~cylinders+displacement+horsepower+weight,data=dataset_train)
Y <- dataset_train$mpg %>% as.matrix()
X_test <- model.matrix(mpg~cylinders+displacement+horsepower+weight,data=dataset_test)
Y_test <- dataset_test$mpg %>% as.matrix()
```

Use the backend to "compile" the model, specifying the optimization algorithm and the objective loss.
```{r}
model %>% compile(loss = "mean_squared_error", optimizer = "sgd")
```

This is the training part. The history output shows us the evolution of the training error.
```{r}
history <- model %>% fit(X, Y, epochs = 200)
history <- history$metrics[[1]]
ggplot(data.frame(x=1:length(history), y=history), aes(x=x, y=y)) + 
    geom_line() + labs(x="Epoch",y="Error")
```

We can now evaluate the final result. 
```{r}
model %>% evaluate(X_test, Y_test)
```

Compute the predictions of the model.
```{r}
y_nn <- model %>% predict_on_batch(X_test) %>% as.array() %>% c()
```


Lets fit a simple polynomial regression model.
```{r}
lm_model <- lm(mpg ~ poly(cylinders,2) + poly(displacement,2) + 
       poly(horsepower,2) + poly(weight,2), data=dataset_train)
y_lm <- predict(lm_model, newdata = dataset_test)
```

Compute and compare the RMSE of the two models.
```{r}
cat("NN RMSE:", sqrt(mean((Y_test - y_nn)^2) ), "\n")
cat("LM RMSE:", sqrt(mean((Y_test - y_lm)^2) ), "\n")
#cat("Both RMSE:", sqrt(mean((Y_test - 0.5*y_lm - 0.5*y_nn)^2) ))
```

Plot the predictions. 
```{r}
library(tidyr)
data_predict <- data.frame(true = Y_test, nn = y_nn, lm = y_lm) %>%
    pivot_longer(c(nn, lm), names_to = "method", values_to = "test_error")

ggplot(data_predict, aes(x=true, y=test_error, color=method)) + geom_point() +
    geom_abline(slope = 1, intercept = 0)
```


# P