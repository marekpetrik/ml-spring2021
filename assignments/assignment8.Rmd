---
title: "Assignment 8"
subtitle: CS 750/850 Machine Learning
output: pdf_document
---

- **Due**: April 5 at 11:59PM
- **Submisssion**: Turn in both a __PDF__ and the __source code__ on [MyCourses](http://mycourses.unh.edu)
- **Questions**: Please see piazza and office hour information on the class website


# Problem 1 [25%]

Using the [MNIST](http://yann.lecun.com/exdb/mnist/) dataset, which we used already in Assignment 2, compare whether boosting, bagging, and random forests work the best. You may may want to use only a subset of the data.

Use [xgboost](https://cran.r-project.org/web/packages/xgboost/index.html) (also available for Python) to see whether the results are better than other boosting methods.

# Problem 2 [25%]

Here we explore the maximal margin classifier on a toy data set.
\begin{enumerate}
	\item[(a)] We are given n = 7 observations in p = 2 dimensions. For each observation, there is an associated class label. Sketch (\emph{by hand is OK}) the observations. \\

	\begin{center}
	\begin{tabular}{|cccc|}
		\hline
		Obs. & $X_1$ & $X_2$ & $Y$ \\
		\hline
		1 & 3 & 4 & Red \\
		2 & 2 & 2 & Red \\
		3 & 4 & 4 & Red \\
		4 & 1 & 4 & Red \\		
		5 & 2 & 1 & Blue \\		
		6 & 4 & 3 & Blue \\		
		7 & 4 & 1 & Blue \\		
		\hline
	\end{tabular}
	\end{center}
	
	\item[(b)] Sketch (\emph{by hand is OK}) the optimal separating hyperplane, and provide the equation for this hyperplane (of the form (9.1)).
	\item[(c)] Describe the classification rule for the maximal margin classifier.
	It should be something along the lines of ``Classify to Red if $\beta_0 + \beta_1 X_1 + \beta_2 X_2 > 0$, and classify to Blue otherwise.'' Provide the values for $\beta_0$, $\beta_1$, and $\beta_2$.
	\item[(d)] On your sketch, indicate the margin for the maximal margin hyperplane.
	\item[(e)] Indicate the support vectors for the maximal margin classifier.  How will the number of support vectors depend on the dimensionality of the space.
	\item[(f)] Argue that a slight movement of the seventh observation would not affect the maximal margin hyperplane.
	\item[(g)] Sketch a hyperplane that is not the optimal separating hyper-plane, and provide the equation for this hyperplane.
	\item[(h)] Draw an additional observation on the plot so that the two classes are no longer separable by a hyperplane.
\end{enumerate}

# Problem 3 [25%]

Generate a simulated two-class data set with 200 observations and two features in which there is a visible but non-linear separation between the two classes. Explore whether in this setting, a support vector machine with a polynomial kernel (with degree greater than 1) or a radial kernel will outperform a support vector classifier on the training data. Which technique performs best on the test data? Make plots and report training and test error rates in order to back up your assertions.


# Problem 4 [25%]

Apply SVMs and at least 3 different kernels to a data set of your choice.  Use cross-validation to optimize the parameter $C$. Be sure to fit the models on a training set and to evaluate their performance on a test set. How accurate are the results compared to simple methods like linear or logistic regression? Which of these approaches yields the best performance?