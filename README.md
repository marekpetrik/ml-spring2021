# CS750 / CS850: Machine Learning #

### When and Where 

Lectures: MW 11:10 am - 12:30 pm

Recitations: F 1:10pm - 2:00pm, online only, led by Soheil

Where: Kingsbury N121 and Online (zoom via mycourses)

**Class recordings:** I will share class recordings only with students who have pre-arranged their absence with me and have a valid reason for not attending at the time of the lecture. However, neither the lectures nor the recitations are mandatory. 

See [class overview](overview/overview.pdf) for the information on grading, rules, and office hours.

## Syllabus: Lectures

The slides will be updated before each lecture. The topics are preliminary and may change due to snow days or due to popular demand.

Regarding *reading materials* see the section on textbooks below. *ISL* is the main textbook.

| Date   | Day | Slides                                                                                    | Reading    | Notebooks
| ------ | --- | ----------------------------------------------------------------------------------------- | ---------- | ---------
| Feb 01 | Mon | [Introduction](slides/class1/class1.pdf)                                                  | ISL 1,2    | [intro](notebooks/class1/introduction.Rmd), [plots](slides/figs/class1/plots.R)
| Feb 03 | Wed | [Linear regression I](slides/class2/class2.pdf)                                           | ISL 3.1-2  | [plots](slides/figs/class2/plots.R), [LR](notebooks/class2/linear_regression.Rmd), 
| Feb 08 | Mon | [Linear regression II](slides/class3/class3.pdf)                                          | ISL 3.3-6  | [bias-var](notebooks/class3/bias_variance.Rmd), [QQ](notebooks/class3/qqplots.Rmd), [tides](notebooks/class3/tides.Rmd)
| Feb 10 | Wed | [Logistic regression](slides/class4/class4.pdf)                                           | ISL 4.1-3  | 
| Feb 15 | Mon | [LDA, QDA](slides/class5/class5.pdf) 		                                                   | ISL 4.4-6  | [MNIST](notebooks/class4/mnist.Rmd) 
| Feb 17 | Wed | [LDA,QDA](slides/class5/class5.pdf),  [Notes](slides/class5/class5_notes.pdf)             | ISL 4.4-6  | [BUG](assignments/mnist_sample.R)
| Feb 22 | Mon | [Maximum likelihood, MAP](slides/class6/class6.pdf)[Demo](slides/class6/maxlikelihood.pdf)|
| Feb 24 | Wed | [Cross-validation](slides/class7/class7.pdf)                                              | ISL 5      |
| Mar 01 | Mon | [Model selection](slides/class8/class8.pdf)                                               | ISL 6.1-6.2|
| Mar 03 | Wed | [Dimensionality](slides/class9/class9.pdf)                                                | ISL 6.3-6.4| [(overfit)](notebooks/class9/overfitting.Rmd)
| Mar 08 | Mon | No lecture; a virtual Friday                                                              |
| Mar 10 | Wed | [Regression splines](slides/class10/class10.pdf)                                          | ISL 7      | [(poly)](notebooks/class10/polynomials.Rmd) [(splines)](notebooks/class10/spline_simple.Rmd)
| Mar 15 | Mon | [Linear algebra](slides/class11/class11_ann.pdf)                                          | LAR           | 
| Mar 17 | Wed | [Linear algebra and ML](slides/class12/class12_ann.pdf)                                   | LAR           |
| Mar 22 | Mon | [Gradient descent, Newton's Method](slides/class12_5/class12_5.pdf)                       | CO 9.2     | [(Optimization)](notebooks/class12_5/optimization.Rmd)
| Mar 24 | Wed | [Midterm review](slides/class_mid/class_mid.pdf)                                          |            
| Mar 29 | Mon | [Decision trees and boosting](slides/class13/class13.pdf)                                 | ISL 8      |
| Mar 31 | Wed | [SVM](slides/class14/class14.pdf)                                                         | ISL 9      | [(Implementation)](notebooks/class14/separating_hyperplane.Rmd)
| Apr 05 | Mon | [SVM](slides/class15/class15.pdf)                                                         | ISL 9      | 
| Apr 07 | Wed | [PCA](slides/class16/class16.pdf)                                                         | ISL 10.1-2 | [(PCA)](notebooks/class16)
| Apr 12 | Mon | [Clustering](slides/class17/class17.pdf)                                                  | ISL 10.3-5 |
| Apr 14 | Wed | [Expectation maximization: Mixtures]()                                                    | TBD        |
| Apr 19 | Mon | [Bayesian machine learning](slides/class19/class19.pdf)                                   | AIMA 14    |
| Apr 21 | Wed | [Bayesian machine learning](slides/class20/class20.pdf)                                   | AIMA 14    | 
| Apr 26 | Mon | [Bayesian machine learning](slides/class20_5/class20_5.pdf)                               | AIMA 14    |[(stan)](notebooks/class20/stan_example.Rmd)
| Apr 28 | Wed | [Recommender systems](slides/class20_7/class20_7.pdf)                                     |            |
| May 03 | Mon | [Deep learning](slides/class21/class21.pdf)                                               | DL| [(keras)](notebooks/class22/keras_example.Rmd)  
| May 05 | Wed | Final review                                                                              |            |
| May 10 | Mon | [Project presentations and discussion](slides/class23/class23.pdf)                        |            |


## Office Hours

*Piazza*: [piazza.com/unh/spring2021/cs750cs850](http://piazza.com/unh/spring2021/cs750cs850)

  - *Marek*: Tue 8:30-9:30am at [https://unh.zoom.us/j/5833938643](https://unh.zoom.us/j/5833938643)
  - *Soheil (TA)*: Fri 2-3pm at [https://unh.zoom.us/j/92604812439 ](https://unh.zoom.us/j/92604812439)
  - *Xihong (TA)*: Mon 1:30-2:30pm at [https://unh.zoom.us/j/94956672072](https://unh.zoom.us/j/94956672072), password: 365608.

## Assignments

| Assignment                         | Source                              |  Due Date           |
|----------------------------------- | ----------------------------------- | --------------------|
| [1](assignments/assignment1.pdf)   | [1](assignments/assignment1.Rmd)    | Mon 2/08 at 11:59PM |
| [2](assignments/assignment2.pdf)   | [2](assignments/assignment2.Rmd)    | Mon 2/15 at 11:59PM |
| [3](assignments/assignment3.pdf)   | [3](assignments/assignment3.Rmd)    | Mon 2/22 at 11:59PM |
| [4](assignments/assignment4.pdf)   | [4](assignments/assignment4.Rmd)    | Mon 3/01 at 11:59PM |
| [5](assignments/assignment5.pdf)   | [5](assignments/assignment5.Rmd)    | Mon 3/15 at 11:59PM |
| [6](assignments/assignment6.pdf)   | [6](assignments/assignment6.Rmd)    | Mon 3/22 at 11:59PM |
| [7](assignments/assignment7.pdf)   | [7](assignments/assignment7.Rmd)    | Mon 3/29 at 11:59PM |
| [8](assignments/assignment8.pdf)   | [8](assignments/assignment8.Rmd)    | Mon 4/05 at 11:59PM |
| [9](assignments/assignment9.pdf)   | [9](assignments/assignment9.Rmd)    | Mon 4/12 at 11:59PM |
| [10](assignments/assignment10.pdf) | [10](assignments/assignment10.Rmd)  | Mon 4/19 at 11:59PM |
| [11](assignments/assignment11.pdf) | [11](assignments/assignment11.Rmd)  | Wed 4/28 at 11:59PM |
| [12](assignments/assignment12.pdf) | [12](assignments/assignment12.Rmd)  | Mon 5/10 at 11:59PM |

## Solutions

We will not be posting worked out solutions this year. Unfortunately, they seem to get used in subsequent years too much. If you would like to see what is the right way of solving a problem you struggled with, please let me know and I will be happy to share the solution with you (if we have one).

## Project

Given the remote character of the class going forward and the additional barriers to collaboration, we will not be working on a joint project/dataset this year. Instead, everyone can choose their own. 

See [Project Description](project/project.pdf) for the requirements and suggestions for the class project. 

## Quizzes

The projected dates for the quizzes are:

| Quiz          |  Dates Available|  
| ------------- |  -------------- |
| Quiz 1        |  2/24-3/04      |
| Quiz 2        |  3/11-3/18      |
| Quiz 3        |  4/12-4/19      |
| Quiz 4        |  4/23-5/04      |

The quizzes will be available online on mycourses. See [practice questions](questions/test_questions.pdf) for the type of questions you may expect to be on the quizzes.

The quizzes are multiple choice and there will be 2 attempts allowed for each quiz. The best result counts.

## Exams

The final exam will be a take-home over the finals week. The exam will be based on conceptual/theoretical questions similar to homework assignments.

## Textbooks ##

### Main Reference:
- **ISL**: [James, G., Witten, D., Hastie, T., & Tibshirani, R. (2013). An Introduction to Statistical Learning](http://www-bcf.usc.edu/~gareth/ISL/)

### In-depth Machine Learning:
- **ESL**: [Hastie, T., Tibshirani, R., & Friedman, J. (2009). The Elements of Statistical Learning. Springer Series in Statistics (2nd ed.)](http://statweb.stanford.edu/~tibs/ElemStatLearn)
- **MLP**: Murphy, K (2012). Machine Learning, A Probabilistic Perspective.
- **ML**: Mitchell, T. (1997). Machine Learning
- Scholkopf B., Smola A. (2001). Learning with Kernels.
- **DL**: Goodfellow, I., Bengio, Y., & Courville, A. (2016). [Deep Learning](http://www.deeplearningbook.org/) 
- **PRM** [Bishop, C. M. (2006). Pattern Recognition and Machine Learning](https://www.microsoft.com/en-us/research/uploads/prod/2006/01/Bishop-Pattern-Recognition-and-Machine-Learning-2006.pdf) 


### Linear Algebra:
- **LAO**: Hefferon, J. [Linear Algebra](http://joshua.smcvt.edu/linearalgebra/#current_version) (2017)
- **LA**: Strang, G. [Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/). (2016) *Also see:* [online lectures](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/)
- **LAR**: [Introductory Linear Algebra with R](http://bendixcarstensen.com/APC/linalg-notes-BxC.pdf)

### Mathematical Optimization:
- **CO** Boyd, S., & Vandenberghe, L. (2004). [Convex Optimization](http://web.stanford.edu/~boyd/cvxbook/)
- Berstsekas, D., (2016), Nonlinear programming, Athena Scientific.
- Nocedal, J., Wright, S. (2006). [Numerical Optimization](https://www.springer.com/us/book/9780387303031)

### Statistics:
- Casella G., & Berger R. L. (2002) Statistical Inference, 2nd edition.
- Dekking, F. M., Kraaikamp, C., Lopuhaa, H. P., & Meester, L. E. (2005). A modern introduction to probability and statistics.

### Related Areas:
- **RL**: Sutton, R. S., & Barto, A. (2018). [Reinforcement learning](http://people.inf.elte.hu/lorincz/Files/RL_2006/SuttonBook.pdf). 2nd edition 
- **RLA**: Szepesvari, C. (2013), [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html)
- **AIMA**: Russell, S., & Norvig, P. (2013). Artificial Intelligence A Modern Approach. (3rd ed.).


## Class Content

The goal of this class is to teach you how to use *machine learning* to *understand data* and *make predictions* in practice. The class will cover the fundamental concepts and algorithms in machine learning and data science as well as a wide variety of practical algorithms. The main topics we will cover are:

1. The maximum likelihood principle
2. *Regression*: Linear regression
3. *Classification*: Logistic regression and linear discriminant analysis
4. *Cross-validation*: bootstrap, and over-fitting
5. *Model selection*: Regularization, Lasso
6. *Nonlinear models*: Decision trees, Support vector machines
7. *Unsupervised*: Principal component analysis, k-means
8. *Advanced topics*: Bayes nets and deep learning

The graduate version of the class will cover the same topics in more depth.

## Programming Language

The class will involve hand-on data analysis using machine learning methods. The recommended language for programming assignments is [R](https://www.r-project.org/) which is an excellent tool for statistical analysis and machine learning. *No prior knowledge of R is needed or expected*; the book and lectures will include a gentle introduction to the language. You can also use Python instead if you are confident that you can install appropriate packages and figure out how to solve assignments. About 15% of the class used Python in 2019.

### R Resources

- [R For Data Science](https://r4ds.had.co.nz/index.html)
- [Cheatsheets](https://www.rstudio.com/resources/cheatsheets/)
- [R Markdown](https://github.com/rstudio/cheatsheets/raw/master/rmarkdown-2.0.pdf)

We recommend using the free [R Studio](https://www.rstudio.com) for completing programming assignments. R Notebooks are very convenient for producing reproducible reports and we encourage you to use them.

If you are more adventurous, you may want to give nvim-r a try. It is a package for vim/nvim that has excellent R support.

### Python Resources

Book code is available for Python, for example, [here](https://github.com/JWarmenhoven/ISLR-python). If you find other good sources, please let me know. [Jupyter](https://jupyter.org/) is a similar alternative for Python. 

## Pre-requisites ##

Basic programming skills (scripting languages like Python are OK) and some familiarity with statistics and calculus. If in doubt, please email the instructor.
